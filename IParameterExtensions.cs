using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using System.Linq;
#endif

namespace ProbeNet.Messages
{
    /// <summary>
    /// Extensions for the <see cref="IParameterDescription"/> interface.
    /// </summary>
    public static class IParameterExtensions
    {
        /// <summary>
        /// Gets a list of parameters that are editable.
        /// </summary>
        /// <returns>The parameters that are editable.</returns>
        /// <param name="parameters">The parameters.</param>
        /// <typeparam name="TParameterDescription">The type of the parameter description enumerable.</typeparam>
#if MICRO_FRAMEWORK
        public static IList GetEditable(this IEnumerable parameters)
        {
            IList result = new ArrayList();
            if (parameters != null) {
                IEnumerator enumerator = parameters.GetEnumerator();
                while (enumerator.MoveNext()) {
                    IParameterDescription parameterDescription = enumerator.Current as IParameterDescription;
                    if (parameterDescription != null && parameterDescription.Editable) {
                        result.Add(parameterDescription);
                    }
                }
            }
            return result;
        }
#else
        public static IList<TParameterDescription> GetEditable<TParameterDescription>(this IEnumerable<TParameterDescription> parameters)
            where TParameterDescription: IParameterDescription
        {
            if(parameters == null) {
                return new List<TParameterDescription>();
            }
            return parameters.Where((p => p.Editable)).ToList();
        }
#endif
    }
}

