#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;
using System.Xml.Linq;
using Frank.Helpers.Json;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Serializer for enum descriptions.
    /// </summary>
    public class EnumDescriptionSerializer<T>: ICustomSerializer<T>
        where T: struct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.EnumDescriptionSerializer`1"/> class.
        /// </summary>
        public EnumDescriptionSerializer ()
        {
        }

#region ICustomSerializer implementation
        /// <inheritdoc/>
        public void SerializeToAttribute (T objectToSerialize, XAttribute attrToFill)
        {
            attrToFill.Value = GetDescription(objectToSerialize);
        }

        /// <inheritdoc/>
        public void SerializeToElement (T objectToSerialize, XElement elemToFill)
        {
            elemToFill.Value = GetDescription(objectToSerialize);
        }

        /// <inheritdoc/>
        public string SerializeToValue (T objectToSerialize)
        {
            return GetDescription(objectToSerialize);
        }

        /// <inheritdoc/>
        public T DeserializeFromAttribute (XAttribute attrib)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public T DeserializeFromElement (XElement element)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public T DeserializeFromValue (string value)
        {
            throw new NotImplementedException();
        }
        #endregion

        private string GetDescription(T objectToSerialize)
        {
            return EnumDescriptionHelper.GetDescription<T>(objectToSerialize);
        }
    }
}
#endif
