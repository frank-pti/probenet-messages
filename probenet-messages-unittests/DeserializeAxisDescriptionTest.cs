using System;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;

namespace ProbeNet.Messages.Unittests
{
    [TestFixture]
    public class DeserializeAxisDescriptionTest
    {
        [Test]
        public void TestDeserialization()
        {
            string jsonString = @"{
              ""caption"": ""Elongation"",
              ""unit"": ""mm"",
              ""id"": ""elongation""
            }";
            ProbeNet.Messages.Raw.AxisDescription deserialized =
                JsonConvert.DeserializeObject<ProbeNet.Messages.Raw.AxisDescription>(jsonString);
            Assert.AreEqual("Elongation", deserialized.Caption);
            Assert.AreEqual("mm", deserialized.Unit);
            Assert.AreEqual("elongation", deserialized.Id);
        }

        [Test]
        public void TestListDeserialization()
        {
            string jsonString = @"[
                {
                  ""caption"": ""Elongation"",
                  ""unit"": ""mm"",
                  ""id"": ""elongation""
                },
                {
                  ""caption"": ""Force"",
                  ""unit"": ""N"",
                  ""id"": ""force""
                }
            ]";
            IList<ProbeNet.Messages.Raw.AxisDescription> deserialized =
                JsonConvert.DeserializeObject<IList<ProbeNet.Messages.Raw.AxisDescription>>(jsonString);
            Assert.AreEqual(2, deserialized.Count);
        }
    }
}

