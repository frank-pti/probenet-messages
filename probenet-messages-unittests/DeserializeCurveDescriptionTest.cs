using System;
using Newtonsoft.Json;
using NUnit.Framework;

namespace ProbeNet.Messages.Unittests
{
    [TestFixture]
    public class DeserializeCurveDescriptionTest
    {
        [Test]
        public void TestDeserialization()
        {
            string jsonString = @"{
              ""coordinateSystem"": ""cartesian"",
              ""axes"": [
                {
                  ""caption"": ""Elongation"",
                  ""unit"": ""mm"",
                  ""id"": ""elongation""
                },
                {
                  ""caption"": ""Force"",
                  ""unit"": ""N"",
                  ""id"": ""force""
                }
              ]
            }";
            ProbeNet.Messages.Raw.CurveDescription deserialized =
                JsonConvert.DeserializeObject<ProbeNet.Messages.Raw.CurveDescription>(jsonString);
            Assert.NotNull(deserialized.Axes);
            Assert.AreEqual(2, deserialized.Axes.Count);
        }
    }
}

