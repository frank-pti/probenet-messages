/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class ParameterDescription :
#if MICRO_FRAMEWORK
 Base.ParameterDescription,
#else
 Base.Generic.ParameterDescription<EnumerationValue>,
#endif
 IParameterDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.ParameterDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="type">Type.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public ParameterDescription(string id, string caption, ParameterType type) :
            this(id, caption, "", type)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.ParameterDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="unit">Unit.</param>
        /// <param name="type">Type.</param>
        public ParameterDescription(string id, string caption, string unit, ParameterType type) :
            base(id, type)
        {
            Caption = caption;
            Unit = unit;
        }

        #region implemented abstract members of ProbeNet.Messages.Base.ParameterDescription
        /// <inheritdoc/>
        protected override string CaptionString
        {
            get
            {
                return Caption;
            }
        }

        /// <inheritdoc/>
        protected override string UnitString
        {
            get
            {
                return Unit;
            }
        }
        #endregion

#if MICRO_FRAMEWORK
        /// <inheritdoc/>
        protected override IList BaseEnumValues
        {
            get { return EnumValues; }
        }

        /// <inheritdoc/>
        public new IList EnumValues
        {
            get;
            set;
        }
#endif

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        public string Unit
        {
            get;
            set;
        }
    }
}