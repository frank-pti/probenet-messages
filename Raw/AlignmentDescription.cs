/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class AlignmentDescription : Base.AlignmentDescription, IAlignmentDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.AlignmentDescription"/> class.
        /// </summary>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public AlignmentDescription() :
            base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.AlignmentDescription"/> class.
        /// </summary>
        /// <param name="hasDirection">If set to <c>true</c> has direction.</param>
        /// <param name="hasSide">If set to <c>true</c> has side.</param>
        public AlignmentDescription(bool hasDirection, bool hasSide) :
            base(hasDirection, hasSide)
        {
        }
    }
}