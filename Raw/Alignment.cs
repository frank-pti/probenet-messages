/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class Alignment : Base.Alignment, IAlignment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Alignment"/> class.
        /// </summary>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public Alignment() :
            base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Alignment"/> class.
        /// </summary>
        /// <param name="side">Side.</param>
        /// <param name="direction">Direction.</param>
#if MICRO_FRAMEWORK
        public Alignment(SampleSide side, int direction) :
            this(side, direction, true, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Alignment"/> class.
        /// </summary>
        /// <param name="side">Side.</param>
        /// <param name="direction">Direction.</param>
        /// <param name="sideValid">Side valid.</param>
        /// <param name="directionValid">Direction valid.</param>
        public Alignment(SampleSide side, int direction, bool sideValid, bool directionValid) :
            base(side, direction, sideValid, directionValid)
#else
        public Alignment(SampleSide? side, int? direction) :           
            base(side, direction)
#endif
        {
        }
    }
}