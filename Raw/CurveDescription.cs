/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class CurveDescription :
#if MICRO_FRAMEWORK
 Base.CurveDescription,
#else
        Base.Generic.CurveDescription<AxisDescription>,
#endif
 ICurveDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.CurveDescription"/> class.
        /// </summary>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public CurveDescription() :
            base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.CurveDescription"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        public CurveDescription(CoordinateSystemType coordinateSystem) :
            base(coordinateSystem)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.CurveDescription"/> class.
        /// </summary>
        /// <param name="axes">Axes.</param>
#if MICRO_FRAMEWORK
        public CurveDescription(IList axes)
        {
            Axes = axes;
        }
#else
        public CurveDescription(IList<AxisDescription> axes):
            base(axes)
        {
        }
#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.CurveDescription"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        /// <param name="axes">Axes.</param>
#if MICRO_FRAMEWORK
        public CurveDescription(CoordinateSystemType coordinateSystem, IList axes) :
            base(coordinateSystem)
        {
            Axes = axes;
        }
#else
        public CurveDescription(CoordinateSystemType coordinateSystem, IList<AxisDescription> axes):
            base(coordinateSystem, axes)
        {
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IList BaseAxes
        {
            get
            {
                return Axes;
            }
        }

        /// <inheritdoc/>
        public new IList Axes
        {
            get;
            set;
        }
#else
        [JsonProperty("axes")]
        [YAXSerializableField]
        [YAXSerializeAs("axes")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "axisDescription")]
        public override IList<AxisDescription> Axes
        {
            get {
                return base.Axes;
            }
            set {
                base.Axes = value;
            }
        }
#endif
    }
}