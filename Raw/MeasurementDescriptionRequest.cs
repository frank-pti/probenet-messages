﻿/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ProbeNet.Messages.Interface;            
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class MeasurementDescriptionRequest : Base.MeasurementDescriptionRequest, IMeasurementDescriptionRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.MeasurementDescriptionRequest"/> class.
        /// </summary>
        /// <param name="index">The index of the measurement description that is requested.</param>
        public MeasurementDescriptionRequest(int index) :
            base(index)
        {
        }

#if MICRO_FRAMEWORK
        /// <summary>
        /// Initializes a new instance and set values according to the specified hashtable
        /// </summary>
        /// <param name="deserialized">The deserialized values.</param>
        public MeasurementDescriptionRequest(Hashtable deserialized):
            base(deserialized)
        {
        }

        /// <inheritdoc/>
#else
        /// <inheritdoc/>
        [YAXSerializableField]
        [YAXSerializeAs("index")]
#endif
        public override int Index
        {
            get;
            set;
        }
    }
}
