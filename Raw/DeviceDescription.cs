using ProbeNet.Messages.Interface;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class DeviceDescription :
#if MICRO_FRAMEWORK
 Base.DeviceDescription,
#else
 Base.Generic.DeviceDescription<MeasurementDescription, SequenceDescription>,
#endif
 IDeviceDescription
    {
#if MICRO_FRAMEWORK
        private IList measurements;
        private IList sequences;
#endif

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.DeviceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public DeviceDescription(string id, Guid uuid) :
            base(id, uuid)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.DeviceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
        /// <param name="caption">Caption.</param>
        public DeviceDescription(string id, Guid uuid, string caption) :
            base(id, uuid)
        {
            Caption = caption;
        }

        #region implemented abstract members of ProbeNet.Messages.Base.DeviceDescription
        /// <inheritdoc/>
        protected override string CaptionString
        {
            get
            {
                return Caption;
            }
        }
        #endregion

        string caption;

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
                return caption;
            }
            set
            {
                caption = value;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new int MeasurementCount
        {
            get
            {
                if (measurements == null)
                {
                    return 0;
                }
                return measurements.Count;
            }
            set
            {
                measurements = new ArrayList();
                for (int i = 0; i < value; i++)
                {
                    measurements.Add(null);
                }
            }
        }

        /// <inheritdoc/>
        protected override int BaseMeasurementCount
        {
            get { return MeasurementCount; }
        }
#else
        [JsonProperty("MeasurementCount", Required = Required.Default, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int BaseMeasurementCount
        {
            get
            {
                if (Measurements == null) {
                    return 0;
                }
                return Measurements.Count;
            }
            set
            {
                Measurements = new List<MeasurementDescription>();
                for (int i = 0; i < value; i++) {
                    Measurements.Add(null);
                }
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new int SequenceCount
        {
            get
            {
                if (sequences == null)
                {
                    return 0;
                }
                return sequences.Count;
            }
            set
            {
                sequences = new ArrayList();
                for (int i = 0; i < value; i++)
                {
                    sequences.Add(null);
                }
            }
        }

        /// <inheritdoc/>
        protected override int BaseSequenceCount
        {
            get { return SequenceCount; }
        }
#else
        [JsonProperty("SequenceCount", Required = Required.Default, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int BaseSequenceCount
        {
            get
            {
                if (Sequences == null) {
                    return 0;
                }
                return Sequences.Count;
            }
            set
            {
                Sequences = new List<SequenceDescription>();
                for (int i = 0; i < value; i++) {
                    Sequences.Add(null);
                }
            }
        }

        /// <summary>
        /// Resolves the measurement references.
        /// </summary>
        public void ResolveMeasurementReferences()
        {
            Dictionary<string, MeasurementDescription> measurements = new Dictionary<string, MeasurementDescription>();
            foreach (MeasurementDescription measurement in Measurements) {
                measurements[measurement.Id] = measurement;
            }
            foreach (SequenceDescription sequence in Sequences) {
                foreach (SequenceDescriptionMeasurementReference reference in sequence.Measurements) {
                    if (measurements.ContainsKey(reference.MeasurementId)) {
                        reference.Measurement = measurements[reference.MeasurementId];
                    }
                }
            }
        }
#endif

#if MICRO_FRAMEWORK
        /// <summary>
        /// Add a measurement description to list
        /// </summary>
        /// <remarks>Sequence description(s) must be added separately.</remarks>
        /// <param name="measurementDescription">The measurement description.</param>
        public void AddMeasurementDescription(MeasurementDescription measurementDescription)
        {
            if (measurements == null)
            {
                measurements = new ArrayList();
            }
            measurements.Add(measurementDescription);
        }

        /// <summary>
        /// Get the measurement description at the specified index
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The measurement description.</returns>
        public MeasurementDescription GetMeasurementDescription(int index)
        {
            return (MeasurementDescription)measurements[index];
        }

        /// <summary>
        /// Add a sequence description to list
        /// </summary>
        /// <param name="sequenceDescription">The sequence description.</param>
        public void AddSequenceDescription(SequenceDescription sequenceDescription)
        {
            if (sequences == null) {
                sequences = new ArrayList();
            }
            sequences.Add(sequenceDescription);
        }

        /// <summary>
        /// Get the sequence description at the specified index
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The measurement description.</returns>
        public SequenceDescription GetSequenceDescription(int index)
        {
            return (SequenceDescription)sequences[index];
        }
#endif
    }
}