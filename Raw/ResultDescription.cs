/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
using ProbeNet.Enums.Converting;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class ResultDescription : Base.ResultDescription, IResultDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.ResultDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="source">Source.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public ResultDescription(string id, string caption, SourceType source) :
            this(id, caption, "", source)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.ResultDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="unit">Unit.</param>
        /// <param name="source">Source.</param>
        public ResultDescription(string id, string caption, string unit, SourceType source) :
            base(id, source)
        {
            Caption = caption;
            Unit = unit;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.ResultDescription"/> class.
        /// </summary>
        /// <param name="other">Other.</param>
        public ResultDescription(IResultDescription other) :
#if MICRO_FRAMEWORK
            this(other.Id, other.Caption, other.Unit, SourceTypeConverter.Deserialize(other.Source))
#else
            this(other.Id, other.Caption, other.Unit, other.Source)
#endif
        {
            DecimalPlaces = other.DecimalPlaces;
            Maximum = other.Maximum;
            Minimum = other.Minimum;
        }

        #region implemented abstract members of ProbeNet.Messages.Base.ResultDescription
        /// <inheritdoc/>
        protected override string CaptionString
        {
            get
            {
                return Caption;
            }
        }

        /// <inheritdoc/>
        protected override string UnitString
        {
            get
            {
                return Unit;
            }
        }
        #endregion

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        public string Unit
        {
            get;
            set;
        }
    }
}