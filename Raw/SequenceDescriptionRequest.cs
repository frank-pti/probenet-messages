/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
    public class SequenceDescriptionRequest : Base.SequenceDescriptionRequest, ISequenceDescriptionRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.SequenceDescriptionRequest"/> class.
        /// </summary>
        /// <param name="index">The index of the sequence that is requested.</param>
        public SequenceDescriptionRequest(int index) :
            base(index)
        {
        }

#if MICRO_FRAMEWORK
        /// <summary>
        /// Initializes a new instance and set the values according to the specified hashatable
        /// </summary>
        /// <param name="deserialized">The deserialized values.</param>
        public SequenceDescriptionRequest(Hashtable deserialized):
            base(deserialized)
        {
        }

        /// <inheritdoc/>
#else
        /// <inheritdoc/>
        [YAXSerializableField]
        [YAXSerializeAs("index")]
#endif
        public override int Index
        {
            get;
            set;
        }
    }
}