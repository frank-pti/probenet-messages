/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;
#endif
using ProbeNet.Messages.Interface;

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class SequenceDescription :
#if MICRO_FRAMEWORK
 Base.SequenceDescription,
#else
 Base.Generic.SequenceDescription<SequenceDescriptionMeasurementReference>,
#endif
 ISequenceDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.SequenceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public SequenceDescription(string id, string caption) :
            base(id)
        {
            Caption = caption;
        }

        #region implemented abstract members of ProbeNet.Messages.Base.SequenceDescription
        /// <inheritdoc/>
        protected override string CaptionString
        {
            get
            {
                return Caption;
            }
        }
        #endregion

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IList BaseMeasurements
#else
        protected override IList<Base.SequenceDescriptionMeasurementReference> BaseMeasurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return Measurements;
#else
                return ListCastAdapter<Raw.SequenceDescriptionMeasurementReference, Base.SequenceDescriptionMeasurementReference>.Build(Measurements);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IList Measurements {
            get;
            set;
        }
#else
        [YAXDontSerialize]
        [JsonProperty("Measurements")]
        public new IList<Raw.SequenceDescriptionMeasurementReference> Measurements
        {
            get
            {
                return base.Measurements;
            }
            set
            {
                base.Measurements = value;
            }
        }
#endif
    }
}