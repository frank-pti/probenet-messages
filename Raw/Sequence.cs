/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class Sequence :
#if MICRO_FRAMEWORK
 Base.Sequence,
#else
 Base.Generic.Sequence<Measurement>,
#endif
 ISequence
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Sequence"/> class.
        /// </summary>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public Sequence() :
            this(Guid.NewGuid(), DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        public Sequence(Guid uuid) :
            this(uuid, DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Sequence"/> class.
        /// </summary>
        /// <param name="moment">Moment.</param>
        public Sequence(DateTime moment) :
            this(Guid.NewGuid(), moment)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        /// <param name="moment">Moment.</param>
        public Sequence(Guid uuid, DateTime moment) :
            base(uuid, moment)
        {
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IDictionary BaseMeasurements
#else
        protected override System.Collections.Generic.IDictionary<string, Base.Measurement> BaseMeasurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return Measurements;
#else
                return DictionaryCastAdapter<string, Raw.Measurement, Base.Measurement>.Build(Measurements);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IDictionary Measurements {
            get;
            set;
        }
#else
        [YAXDontSerialize]
        [JsonProperty("Measurements")]
        public new IDictionary<string, Raw.Measurement> Measurements
        {
            get
            {
                return base.Measurements;
            }
            set
            {
                base.Measurements = value;
            }
        }
#endif
    }
}