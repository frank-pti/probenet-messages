/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class SequenceDescriptionMeasurementReference :
#if MICRO_FRAMEWORK
 Base.SequenceDescriptionMeasurementReference,
#else
        Base.Generic.SequenceDescriptionMeasurementReference<MeasurementDescription>,
#endif
 ISequenceDescriptionMeasurementReference
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.Raw.SequenceDescriptionMeasurementReference"/> class.
        /// </summary>
        /// <param name="designation">Designation.</param>
        /// <param name="measurementId">Measurement identifier.</param>
        /// 
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public SequenceDescriptionMeasurementReference(string designation, string measurementId) :
            base(designation, measurementId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.Raw.SequenceDescriptionMeasurementReference"/> class.
        /// </summary>
        /// <param name="designation">Designation.</param>
        /// <param name="measurement">Measurement.</param>
        public SequenceDescriptionMeasurementReference(string designation, MeasurementDescription measurement) :
            base(designation, measurement.Id)
        {
            Measurement = measurement;
        }

#if MICRO_FRAMEWORK
        /// <inheritdoc/>
        protected override Base.MeasurementDescription BaseMeasurement
        {
            get { return Measurement; }
        }

        /// <inheritdoc/>
        public new MeasurementDescription Measurement
        {
            get;
            set;
        }
#endif
    }
}