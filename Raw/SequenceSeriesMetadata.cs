/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class SequenceSeriesMetadata :
#if MICRO_FRAMEWORK
 Base.SequenceSeriesMetadata,
#else
 Base.Generic.SequenceSeriesMetadata<MeasurementMetadata>,
#endif
 ISequenceSeriesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.SequenceSeriesMetadata"/> class.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        /// <param name="uuid">UUID.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public SequenceSeriesMetadata(string sequence, Guid uuid) :
            base(sequence, uuid)
        {
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IDictionary BaseMeasurements
#else
        protected override IDictionary<string, Base.MeasurementMetadata> BaseMeasurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return Measurements;
#else
                return DictionaryCastAdapter<string, Raw.MeasurementMetadata, Base.MeasurementMetadata>.Build(Measurements);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IDictionary Measurements {
            get;
            set;
        }
#else
        [JsonProperty("Measurements")]
        [YAXDontSerialize]
        public new IDictionary<string, Raw.MeasurementMetadata> Measurements
        {
            get
            {
                return base.Measurements;
            }
            set
            {
                base.Measurements = value;
            }
        }
#endif
    }
}