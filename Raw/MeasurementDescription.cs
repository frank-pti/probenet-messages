/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class MeasurementDescription :
#if MICRO_FRAMEWORK
 Base.MeasurementDescription,
#else
 Base.Generic.MeasurementDescription<AlignmentDescription, ParameterDescription, ResultDescription, CurveDescription, ImageDescription>,
#endif
 IMeasurementDescription
    {
        private string shortCaption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.MeasurementDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="shortCaption">Short caption.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public MeasurementDescription(string id, string caption, string shortCaption) :
            base(id)
        {
            Caption = caption;
            ShortCaption = shortCaption;
        }

        #region implemented abstract members of ProbeNet.Messages.Base.MeasurementDescription
        /// <inheritdoc/>
        protected override string CaptionString
        {
            get
            {
                return Caption;
            }
        }

        /// <inheritdoc/>
        protected override string ShortCaptionString
        {
            get
            {
                return ShortCaption;
            }
        }
        #endregion

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("shortCaption")]
        [YAXSerializableField]
        [YAXSerializeAs("shortCaption")]
        [YAXAttributeForClass]
#endif
        public string ShortCaption
        {
            get
            {
                return shortCaption;
            }
            set
            {
                if (value != null && value.Length > Constants.MaximumShortCaptionLength) {
#if MICRO_FRAMEWORK
                    throw new ArgumentException(
                        "The short caption must not be longer than " + Constants.MaximumShortCaptionLength + " characters.", "value");
#else
                    throw new ArgumentException(
                        String.Format("The short caption must not be longer than {0} characters.", Constants.MaximumShortCaptionLength), "value");
#endif
                }
                shortCaption = value;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override Base.AlignmentDescription BaseAlignmentDescription
        {
            get { return AlignmentDescription; }
        }

        public new AlignmentDescription AlignmentDescription
        {
            get;
            set;
        }
#else
        [JsonProperty("alignmentDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("alignmentDescription")]
        public override AlignmentDescription AlignmentDescription
        {
            get
            {
                return base.AlignmentDescription;
            }
            set
            {
                base.AlignmentDescription = value;
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override Base.CurveDescription BaseCurveDescription
        {
            get { return CurveDescription; }
        }

        public new CurveDescription CurveDescription
        {
            get;
            set;
        }
#else
        [JsonProperty("curveDescription")]
        [YAXSerializableField]
        public override CurveDescription CurveDescription
        {
            get
            {
                return base.CurveDescription;
            }
            set
            {
                base.CurveDescription = value;
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IList BaseImageDescriptions
#else
        protected override IList<Base.ImageDescription> BaseImageDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ImageDescriptions;
#else
                return ListCastAdapter<Raw.ImageDescription, Base.ImageDescription>.Build(ImageDescriptions);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IList ImageDescriptions {
            get;
            set;
        }
#else
        [JsonProperty("ImageDescriptions")]
        public new IList<Raw.ImageDescription> ImageDescriptions
        {
            get
            {
                return base.ImageDescriptions;
            }
            set
            {
                base.ImageDescriptions = value;
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IList BaseParameterDescriptions
#else
        protected override IList<Base.ParameterDescription> BaseParameterDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ParameterDescriptions;
#else
                return ListCastAdapter<Raw.ParameterDescription, Base.ParameterDescription>.Build(ParameterDescriptions);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IList ParameterDescriptions {
            get;
            set;
        }
#else
        [JsonProperty("ParameterDescriptions")]
        public new IList<Raw.ParameterDescription> ParameterDescriptions
        {
            get
            {
                return base.ParameterDescriptions;
            }
            set
            {
                base.ParameterDescriptions = value;
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        protected override IList BaseResultDescriptions
#else
        protected override IList<Base.ResultDescription> BaseResultDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ResultDescriptions;
#else
                return ListCastAdapter<Raw.ResultDescription, Base.ResultDescription>.Build(ResultDescriptions);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public new IList ResultDescriptions {
            get;
            set;
        }
#else
        [JsonProperty("ResultDescriptions")]
        public new IList<Raw.ResultDescription> ResultDescriptions
        {
            get
            {
                return base.ResultDescriptions;
            }
            set
            {
                base.ResultDescriptions = value;
            }
        }
#endif
    }
}