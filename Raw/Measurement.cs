/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Raw
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class Measurement : Base.Measurement, IMeasurement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Measurement"/> class.
        /// </summary>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public Measurement() :
            this(Guid.NewGuid())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Raw.Measurement"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        public Measurement(Guid uuid) :
            base(uuid)
        {
#if MICRO_FRAMEWORK
            Results = new Hashtable();
#else
			Results = new Dictionary<string, Nullable<double>>();
#endif
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("alignment", NullValueHandling = NullValueHandling.Ignore)]
        [YAXSerializableField]
        [YAXSerializeAs("alignment")]
#endif
        public Alignment Alignment
        {
            get;
            set;
        }

        /// <inheritdoc/>
        protected override ProbeNet.Messages.Base.Alignment BaseAlignment
        {
            get
            {
                return Alignment;
            }
        }
    }
}