#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Enumerator for the dictonary cast adapter.
    /// </summary>
    public class DictionaryCastEnumerator<TKey, TFrom, TTo>:
        System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey, TTo>>
        where TFrom: class
        where TTo: class
    {
        private System.Collections.Generic.IDictionary<TKey, TFrom> dictionary;
        private System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey, TFrom>> enumerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.DictionaryCastEnumerator`3"/> class.
        /// </summary>
        /// <param name="dictionary">Dictionary.</param>
        public DictionaryCastEnumerator (System.Collections.Generic.IDictionary<TKey, TFrom> dictionary)
        {
            this.dictionary = dictionary;
            this.enumerator = this.dictionary.GetEnumerator();
        }

#region IEnumerator[T] implementation
        /// <inheritdoc/>
        System.Collections.Generic.KeyValuePair<TKey, TTo> System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey, TTo>>.Current {
            get {
                System.Collections.Generic.KeyValuePair<TKey, TFrom> pair = enumerator.Current;
                return new System.Collections.Generic.KeyValuePair<TKey, TTo>(pair.Key, pair.Value as TTo);
            }
        }
        #endregion

#region IEnumerator implementation
        /// <inheritdoc/>
        public bool MoveNext()
        {
            return enumerator.MoveNext();
        }

        /// <inheritdoc/>
        public void Reset()
        {
            enumerator.Reset();
        }
    
        object System.Collections.IEnumerator.Current {
            get {
                return (this as System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<TKey, TTo>>).Current;
            }
        }
        #endregion
    
#region IDisposable implementation
        /// <inheritdoc/>
        public void Dispose()
        {
            enumerator.Dispose();
        }
        #endregion
    }
}
#endif
