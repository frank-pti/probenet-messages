/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
#endif

namespace ProbeNet.Messages.Discovery
{
    /// <summary>
    /// ProbeNet discovery request message.
    /// </summary>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
#endif
    public class Request
    {
        private int port;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Discovery.Request"/> class.
        /// </summary>
        /// <param name="port">Port.</param>
#if !MICRO_FRAMEWORK
        [JsonConstructor]
#endif
        public Request(int port)
        {
            this.port = port;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Discovery.Request"/> class.
        /// </summary>
        /// <param name="port">Port.</param>
        public Request(long port) :
            this((int)port)
        {
        }

        /// <summary>
        /// Gets or sets the port that should be used for answering this request.
        /// </summary>
        /// <value>The port.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("discoveryAnswerPort")]
#endif
        public int Port
        {
            get
            {
                return this.port;
            }
            set
            {
                port = value;
            }
        }

        /// <summary>
        /// Gets or sets the type which is always the type of a discovery request message.
        /// </summary>
        /// <value>The type.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("type")]
#endif
        public string Type
        {
            get
            {
                return Constants.DiscoveryRequestMessageType;
            }
            set
            {
                if (value != Constants.DiscoveryRequestMessageType) {
#if MICRO_FRAMEWORK
                    string message = "Type argument should be \"" + Constants.DiscoveryRequestMessageType +
                        "\", but was \"" + value + "\"";
#else
                    string message = String.Format("Type argument should be \"{0}\", but was \"{1}\"", Constants.DiscoveryRequestMessageType, value);
#endif
                    throw new ArgumentException(message);
                }
            }
        }
    }
}

