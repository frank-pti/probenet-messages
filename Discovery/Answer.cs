/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
#endif

namespace ProbeNet.Messages.Discovery
{
    /// <summary>
    /// ProbeNet Discovery answer message.
    /// </summary>
#if !MICRO_FRAMEWORK
	[JsonObject(MemberSerialization.OptIn)]
#endif
    public class Answer
    {
        private int port;
        private Guid uuid;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Discovery.Answer"/> class.
        /// </summary>
        /// <param name="port">Port.</param>
        /// <param name="uuid">UUID.</param>
        public Answer(int port, Guid uuid)
        {
            this.port = port;
            this.uuid = uuid;
        }

        /// <summary>
        /// Gets or sets the port which should be used for initiating a ProbeNet TCP connection.
        /// </summary>
        /// <value>The TCP port.</value>
#if !MICRO_FRAMEWORK
		[JsonProperty("probeNetTcpPort")]
#endif
        public int Port
        {
            get
            {
                return this.port;
            }
            set
            {
                port = value;
            }
        }

        /// <summary>
        /// Gets or sets the type which always is the type of a discovery answer message.
        /// </summary>
        /// <value>The type.</value>
#if !MICRO_FRAMEWORK
		[JsonProperty("type")]
#endif
        public string Type
        {
            get
            {
                return Constants.DiscoveryAnswerMessageType;
            }
            set
            {
                if (value != Constants.DiscoveryAnswerMessageType) {
#if MICRO_FRAMEWORK
                    string message = "Type argument should be \"" + Constants.DiscoveryAnswerMessageType +
                        "\", but was \"" + value + "\"";
#else
                    string message = String.Format("Type argument should be \"{0}\", but was \"{1}\"", Constants.DiscoveryAnswerMessageType, value);
#endif
                    throw new ArgumentException(message);
                }
            }
        }

        /// <summary>
        /// Gets or sets the UUID.
        /// </summary>
        /// <value>The UUID.</value>
#if !MICRO_FRAMEWORK
		[JsonProperty("uuid")]
#endif
        public Guid Uuid
        {
            get
            {
                return this.uuid;
            }
            set
            {
                uuid = value;
            }
        }
    }
}

