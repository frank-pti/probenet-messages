using Newtonsoft.Json;

namespace ProbeNet.Messages
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ResultColumnSetting
    {
        [JsonConstructor]
        public ResultColumnSetting(string id) :
            this(id, true)
        {
        }

        public ResultColumnSetting(string id, bool visible)
        {
            Id = id;
            Visible = visible;
        }

        [JsonProperty("visible")]
        public bool Visible
        {
            get;
            set;
        }

        [JsonProperty("id")]
        public string Id
        {
            get;
            private set;
        }
    }
}

