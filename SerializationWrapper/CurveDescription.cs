/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ProbeNet.Messages.Interface;

#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
using ProbeNet.Enums;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a curve description.
    /// </summary>
    /// <seealso cref="ICurveDescription"/>
#if MICRO_FRAMEWORK
    public class CurveDescription : Wrapper, ICurveDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class CurveDescription : Wrapper<ICurveDescription>, ICurveDescription
#endif

    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.CurveDescription"/> class.
        /// </summary>
        public CurveDescription() :
            this(new Raw.CurveDescription())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.CurveDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public CurveDescription(ICurveDescription wrapped) :
            base(wrapped)
        {
        }

        #region ICurveDescription implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public System.Collections.IList Axes
#else
        [JsonProperty("axes")]
        [YAXSerializableField]
        [YAXSerializeAs("axes")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "axisDescription")]
        public System.Collections.Generic.IList<IAxisDescription> Axes
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                ICurveDescription wrapped = (ICurveDescription)Wrapped;
#else
                ICurveDescription wrapped = Wrapped;
#endif
                if (wrapped.Axes == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                return wrapped.Axes;
#else
                return wrapped.Axes.Select(a => (IAxisDescription)(new AxisDescription(a))).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string CoordinateSystem
        {
            get
            {
                return ((ICurveDescription)Wrapped).CoordinateSystem;
            }
        }
#else
        [JsonProperty("coordinateSystem")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<CoordinateSystemType>))]
        [YAXSerializableField]
        [YAXSerializeAs("coordinateSystem")]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<CoordinateSystemType>))]
        public ProbeNet.Enums.CoordinateSystemType CoordinateSystem
        {
            get
            {
                return Wrapped.CoordinateSystem;
            }
        }
#endif
        #endregion
    }
}

