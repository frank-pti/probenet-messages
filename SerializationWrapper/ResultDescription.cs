using ProbeNet.Enums;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a result description.
    /// </summary>
#if MICRO_FRAMEWORK
    public class ResultDescription : Wrapper, IResultDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class ResultDescription : Wrapper<IResultDescription>, IResultDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.ResultDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public ResultDescription(IResultDescription wrapped) :
            base(wrapped)
        {
        }

        #region IResultDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).Caption;
#else
                return Wrapped.Caption;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Source
        {
            get
            {
                return ((IResultDescription)Wrapped).Source;
            }
        }
#else
        [JsonProperty("source")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<SourceType>))]
        [YAXSerializableField]
        [YAXSerializeAs("source")]
        [YAXAttributeForClass]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<SourceType>))]
        public SourceType Source
        {
            get
            {
                return Wrapped.Source;
            }
        }
#endif

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        public string Unit
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).Unit;
#else
                return Wrapped.Unit;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("minimum")]
        [YAXSerializableField]
        [YAXSerializeAs("minimum")]
        [YAXAttributeForClass]
#endif
        public double Minimum
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).Minimum;
#else
                return Wrapped.Minimum;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("maximum")]
        [YAXSerializableField]
        [YAXSerializeAs("maximum")]
        [YAXAttributeForClass]
#endif
        public double Maximum
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).Maximum;
#else
                return Wrapped.Maximum;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("decimalPlaces")]
        [YAXSerializableField]
        [YAXSerializeAs("decimalPlaces")]
        [YAXAttributeForClass]
#endif
        public int DecimalPlaces
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IResultDescription)Wrapped).DecimalPlaces;
#else
                return Wrapped.DecimalPlaces;
#endif
            }
        }
        #endregion
    }
}

