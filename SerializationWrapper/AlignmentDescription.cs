/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps an alignment description.
    /// </summary>
    /// <seealso cref="IAlignmentDescription"/>
#if MICRO_FRAMEWORK
    public class AlignmentDescription : Wrapper, IAlignmentDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class AlignmentDescription: Wrapper<IAlignmentDescription>, IAlignmentDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.AlignmentDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public AlignmentDescription(IAlignmentDescription wrapped) :
            base(wrapped)
        {
        }

        #region IAlignmentDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("hasDirection")]
        [YAXSerializableField]
        [YAXSerializeAs("hasDirection")]
        [YAXAttributeForClass]
#endif
        public bool HasDirection
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IAlignmentDescription)Wrapped).HasDirection;
#else
                return Wrapped.HasDirection;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IAlignmentDescription)Wrapped).HasDirection = value;
#else
                Wrapped.HasDirection = value;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("hasSide")]
        [YAXSerializableField]
        [YAXSerializeAs("hasSide")]
        [YAXAttributeForClass]
#endif
        public bool HasSide
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IAlignmentDescription)Wrapped).HasSide;
#else
                return Wrapped.HasSide;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IAlignmentDescription)Wrapped).HasSide = value;
#else
                Wrapped.HasSide = value;
#endif
            }
        }
        #endregion
    }
}

