using ProbeNet.Enums;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps an error.
    /// </summary>
#if MICRO_FRAMEWORK
    public class Error : Wrapper, IError
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class Error : Wrapper<IError>, IError
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.Error"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public Error(IError wrapped) :
            base(wrapped)
        {
        }

        #region IError implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("errorType")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<ErrorType>))]
#endif
        public ErrorType ErrorType
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IError)Wrapped).ErrorType;
#else
                return Wrapped.ErrorType;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty ("message")]
        [YAXSerializableField]
        [YAXSerializeAs ("message")]
        [YAXAttributeForClass]
#endif
        public string Message
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IError)Wrapped).Message;
#else
                return Wrapped.Message;
#endif
            }
        }
        #endregion

        #region IMessageBody implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Type
#else
        [JsonProperty("type")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<MessageType>))]
        public MessageType Type
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IError)Wrapped).Type;
#else
                return Wrapped.Type;
#endif
            }
        }
        #endregion
    }
}

