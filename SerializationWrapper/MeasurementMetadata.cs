/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a measurement metadata.
    /// </summary>
    /// <seealso cref="IMeasurementMetadata"/>
#if MICRO_FRAMEWORK
    public class MeasurementMetadata : Wrapper, IMeasurementMetadata
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class MeasurementMetadata : Wrapper<IMeasurementMetadata>, IMeasurementMetadata
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.MeasurementMetadata"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public MeasurementMetadata(IMeasurementMetadata wrapped) :
            base(wrapped)
        {
        }

        #region IMeasurementMetadata implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("enabled")]
        [YAXSerializeAs("enabled")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public bool Enabled
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementMetadata)Wrapped).Enabled;
#else
                return Wrapped.Enabled;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IMeasurementMetadata)Wrapped).Enabled = value;
#else
                Wrapped.Enabled = value;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary Parameters
#else
        [JsonProperty("parameters")]
        [YAXSerializeAs("parameters")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "parameter",
            KeyName = "id",
            SerializeKeyAs = YAXNodeTypes.Attribute,
            ValueName = "value",
            SerializeValueAs = YAXNodeTypes.Attribute)]
        public IDictionary<string, object> Parameters
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementMetadata)Wrapped).Parameters;
#else
                return Wrapped.Parameters;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IMeasurementMetadata)Wrapped).Parameters = value;
#else
                Wrapped.Parameters = value;
#endif
            }
        }
        #endregion
    }
}

