/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps an image description.
    /// </summary>
    /// <seealso cref="IImageDescription"/>
#if MICRO_FRAMEWORK
    public class ImageDescription : Wrapper, IImageDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class ImageDescription : Wrapper<IImageDescription>, IImageDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.ImageDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public ImageDescription(IImageDescription wrapped) :
            base(wrapped)
        {
        }

        #region IImageDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IImageDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }
        #endregion
    }
}

