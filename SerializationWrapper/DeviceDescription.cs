/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a device description.
    /// </summary>
    /// <see cref="IDeviceDescription"/>
#if MICRO_FRAMEWORK
    public class DeviceDescription : Wrapper, IDeviceDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class DeviceDescription : Wrapper<IDeviceDescription>, IDeviceDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.DeviceDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public DeviceDescription(IDeviceDescription wrapped) :
            base(wrapped)
        {
        }

        #region IDeviceDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Caption;
#else
                return Wrapped.Caption;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("probeNetVersion")]
#endif
        public int ProbeNetVersion
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).ProbeNetVersion;
#else
                return Wrapped.ProbeNetVersion;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("modelNumber")]
#endif
        public string ModelNumber
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).ModelNumber;
#else
                return Wrapped.ModelNumber;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("serialNumber")]
#endif
        public string SerialNumber
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).SerialNumber;
#else
                return Wrapped.SerialNumber;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("firmwareVersion")]
#endif
        public string FirmwareVersion
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).FirmwareVersion;
#else
                return Wrapped.FirmwareVersion;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("manufacturer")]
#endif
        public string Manufacturer
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).FirmwareVersion;
#else
                return Wrapped.Manufacturer;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList Extensions
#else
        [JsonProperty("extensions")]
        public IList<string> Extensions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Extensions;
#else
                return Wrapped.Extensions;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public int SequenceCount
#else
        [JsonProperty("sequences")]
        public IList<ISequenceDescription> Sequences
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IDeviceDescription wrapped = (IDeviceDescription)Wrapped;
                return wrapped.SequenceCount;
#else
                IDeviceDescription wrapped = Wrapped;
                if (wrapped.Sequences == null) {
                    return null;
                }
                return wrapped.Sequences.Select(a => (ISequenceDescription)(new SequenceDescription(a))).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public int MeasurementCount
#else
        [JsonProperty("measurements")]
        public IList<IMeasurementDescription> Measurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IDeviceDescription wrapped = (IDeviceDescription)Wrapped;
                return wrapped.MeasurementCount;
#else
                IDeviceDescription wrapped = Wrapped;
                if (wrapped.Measurements == null) {
                    return null;
                }
                return wrapped.Measurements.Select(a => (IMeasurementDescription)(new MeasurementDescription(a))).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList InstalledLanguages
#else
        [JsonProperty("installedLanguages")]
        public IList<string> InstalledLanguages
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).InstalledLanguages;
#else
                return Wrapped.InstalledLanguages;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("icon")]
#endif
        public byte[] Icon
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Icon;
#else
                return Wrapped.Icon;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("uuid")]
#endif
        public Guid Uuid
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Uuid;
#else
                return Wrapped.Uuid;
#endif
            }
        }
        #endregion

        #region IMessageBody implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Type
#else
        [JsonProperty("type")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<MessageType>))]
        public MessageType Type
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IDeviceDescription)Wrapped).Type;
#else
                return Wrapped.Type;
#endif
            }
        }
        #endregion
    }
}

