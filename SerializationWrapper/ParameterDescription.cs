/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a parameter description.
    /// </summary>
    /// <seealso cref="IParameterDescription"/>
#if MICRO_FRAMEWORK
    public class ParameterDescription : Wrapper, IParameterDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class ParameterDescription : Wrapper<IParameterDescription>, IParameterDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.ParameterDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public ParameterDescription(IParameterDescription wrapped) :
            base(wrapped)
        {
        }

        #region IParameterDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Caption;
#else
                return Wrapped.Caption;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        public string Unit
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Unit;
#else
                return Wrapped.Unit;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Type
        {
            get
            {
                return ((IParameterDescription)Wrapped).Type;
            }
        }
#else
        [JsonProperty("type")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<ParameterType>))]
        [YAXSerializableField]
        [YAXSerializeAs("type")]
        [YAXAttributeForClass]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<ParameterType>))]
        public ParameterType Type
        {
            get
            {
                return Wrapped.Type;
            }
        }
#endif

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("minimum")]
        [YAXSerializableField]
        [YAXSerializeAs("minimum")]
        [YAXAttributeForClass]
#endif
        public double Minimum
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Minimum;
#else
                return Wrapped.Minimum;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("maximum")]
        [YAXSerializableField]
        [YAXSerializeAs("maximum")]
        [YAXAttributeForClass]
#endif
        public double Maximum
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Maximum;
#else
                return Wrapped.Maximum;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("decimalPlaces")]
        [YAXSerializableField]
        [YAXSerializeAs("decimalPlaces")]
        [YAXAttributeForClass]
#endif
        public int DecimalPlaces
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).DecimalPlaces;
#else
                return Wrapped.DecimalPlaces;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("editable")]
        [YAXSerializableField]
        [YAXSerializeAs("editable")]
        [YAXAttributeForClass]
#endif
        public bool Editable
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IParameterDescription)Wrapped).Editable;
#else
                return Wrapped.Editable;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList EnumValues
#else
        [JsonProperty("enumValues")]
        [YAXSerializableField]
        [YAXSerializeAs("enumValues")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "enumValue")]
        public IList<IEnumerationValue> EnumValues
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IParameterDescription wrapped = (IParameterDescription)Wrapped;
#else
                IParameterDescription wrapped = Wrapped;
#endif
                if (wrapped.EnumValues == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                return wrapped.EnumValues;
#else
                return wrapped.EnumValues.Select(e => (IEnumerationValue)new EnumerationValue(e)).ToList();
#endif
            }
        }
        #endregion
    }
}

