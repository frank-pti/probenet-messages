/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a sequence description.
    /// </summary>
#if MICRO_FRAMEWORK
    public class SequenceDescription : Wrapper, ISequenceDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class SequenceDescription : Wrapper<ISequenceDescription>, ISequenceDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.SequenceDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public SequenceDescription(ISequenceDescription wrapped) :
            base(wrapped)
        {
        }

        #region ISequenceDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializeAs("id")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceDescription)Wrapped).Caption;
#else
                return Wrapped.Caption;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public System.Collections.IList Measurements
#else
        [JsonProperty("measurements")]
        [YAXSerializeAs("measurements")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "measurement")]
        public System.Collections.Generic.IList<ISequenceDescriptionMeasurementReference> Measurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                ISequenceDescription wrapped = (ISequenceDescription)Wrapped;
#else
                ISequenceDescription wrapped = Wrapped;
#endif
                if (wrapped.Measurements == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                return wrapped.Measurements;
#else
                return wrapped.Measurements.Select(r => (ISequenceDescriptionMeasurementReference)(new SequenceDescriptionMeasurementReference(r))).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("canBeInitiatedRemotely")]
        [YAXSerializeAs("canBeInitiatedRemotely")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public bool CanBeInitiatedRemotely
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceDescription)Wrapped).CanBeInitiatedRemotely;
#else
                return Wrapped.CanBeInitiatedRemotely;
#endif
            }
        }
        #endregion
    }
}

