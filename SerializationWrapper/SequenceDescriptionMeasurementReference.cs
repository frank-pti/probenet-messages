/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a sequence description measurement reference.
    /// </summary>
    /// <seealso cref="ISequenceDescriptionMeasurementReference"/>
#if MICRO_FRAMEWORK
    public class SequenceDescriptionMeasurementReference : Wrapper, ISequenceDescriptionMeasurementReference
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class SequenceDescriptionMeasurementReference : Wrapper<ISequenceDescriptionMeasurementReference>, ISequenceDescriptionMeasurementReference
#endif
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.SerializationWrapper.SequenceDescriptionMeasurementReference"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public SequenceDescriptionMeasurementReference(ISequenceDescriptionMeasurementReference wrapped) :
            base(wrapped)
        {
        }

        #region ISequenceDescriptionMeasurementReference implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("designation")]
        [YAXSerializeAs("designation")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string Designation
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceDescriptionMeasurementReference)Wrapped).Designation;
#else
                return Wrapped.Designation;
#endif
            }
        }

        /// <inheritdoc/>
        public IMeasurementDescription Measurement
        {
            get
            {
#if MICRO_FRAMEWORK
                ISequenceDescriptionMeasurementReference wrapped = (ISequenceDescriptionMeasurementReference)Wrapped;
#else
                ISequenceDescriptionMeasurementReference wrapped = Wrapped;
#endif
                if (wrapped.Measurement == null) {
                    return null;
                }
                return new MeasurementDescription(wrapped.Measurement);
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("measurementId")]
        [YAXSerializeAs("measurement")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string MeasurementId
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceDescriptionMeasurementReference)Wrapped).Measurement.Id;
#else
                return Wrapped.Measurement.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList Depends
#else
        [JsonProperty("depends")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "depends")]
        public IList<string> Depends
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                ISequenceDescriptionMeasurementReference wrapped = (ISequenceDescriptionMeasurementReference)Wrapped;
#else
                ISequenceDescriptionMeasurementReference wrapped = Wrapped;
#endif
                if (wrapped.Depends == null) {
                    return null;
                }
                return wrapped.Depends;
            }
        }
        #endregion
    }
}

