using ProbeNet.Enums;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps an alignment.
    /// </summary>
    /// <seealso cref="IAlignment"/>
#if MICRO_FRAMEWORK
    public class Alignment : Wrapper, IAlignment
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class Alignment : Wrapper<IAlignment>, IAlignment
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.SerializationWrapper.Alignment"/> class.
        /// </summary>
        /// <param name="alignment">Alignment.</param>
        public Alignment(IAlignment alignment) :
            base(alignment)
        {
        }

        #region IEquatable implementation
        /// <summary>
        /// Determines whether the specified <see cref="ProbeNet.Messages.Interface.IAlignment"/> is equal to the
        /// current <see cref="ProbeNet.Messages.SerializationWrapper.Alignment"/>.
        /// </summary>
        /// <param name="other">The <see cref="ProbeNet.Messages.Interface.IAlignment"/> to compare with the current <see cref="ProbeNet.Messages.SerializationWrapper.Alignment"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="ProbeNet.Messages.Interface.IAlignment"/> is equal to the current
        /// <see cref="ProbeNet.Messages.SerializationWrapper.Alignment"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(IAlignment other)
        {
            return Side == other.Side && Direction == other.Direction;
        }
        #endregion

        #region IAlignment implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public bool DirectionValid
        {
            get
            {
                return ((IAlignment)Wrapped).DirectionValid;
            }
            set
            {
                ((IAlignment)Wrapped).DirectionValid = value;
            }
        }

        /// <inheritdoc/>
        public int Direction
#else
        [JsonProperty("direction", NullValueHandling = NullValueHandling.Ignore)]
        [YAXSerializeAs("direction")]
        [YAXSerializableField]
        public int? Direction
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IAlignment)Wrapped).Direction;
#else
                return Wrapped.Direction;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IAlignment)Wrapped).Direction = value;
#else
                Wrapped.Direction = value;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public bool SideValid
        {
            get
            {
                return ((IAlignment)Wrapped).SideValid;
            }
            set
            {
                ((IAlignment)Wrapped).SideValid = value;
            }
        }

        /// <inheritdoc/>
        public SampleSide Side
#else
        [JsonProperty("side", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(JsonDescriptionNullableEnumConverter<SampleSide>))]
        [YAXSerializeAs("side")]
        [YAXSerializableField]
        [YAXCustomSerializer(typeof(NullableEnumDescriptionSerializer<SampleSide>))]
        public SampleSide? Side
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IAlignment)Wrapped).Side;
#else
                return Wrapped.Side;
#endif
            }
            set
            {
#if MICRO_FRAMEWORK
                ((IAlignment)Wrapped).Side = value;
#else
                Wrapped.Side = value;
#endif
            }
        }
        #endregion
    }
}