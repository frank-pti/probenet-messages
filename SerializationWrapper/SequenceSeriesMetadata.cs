/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wrapper for sequence series metadata.
    /// </summary>
#if MICRO_FRAMEWORK
    public class SequenceSeriesMetadata : Wrapper, ISequenceSeriesMetadata
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class SequenceSeriesMetadata : Wrapper<ISequenceSeriesMetadata>, ISequenceSeriesMetadata
#endif
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.SerializationWrapper.SequenceSeriesMetadata"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public SequenceSeriesMetadata(ISequenceSeriesMetadata wrapped) :
            base(wrapped)
        {
        }

        #region ISequenceSeriesMetadata implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("sequence")]
        [YAXSerializeAs("sequence")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string Sequence
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceSeriesMetadata)Wrapped).Sequence;
#else
                return Wrapped.Sequence;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("uuid")]
        [YAXSerializeAs("uuid")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public Guid Uuid
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceSeriesMetadata)Wrapped).Uuid;
#else
                return Wrapped.Uuid;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary Measurements
#else
        [JsonProperty("measurements")]
        [YAXSerializeAs("measurements")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "measurement",
            KeyName = "designation",
            SerializeKeyAs = YAXNodeTypes.Attribute,
            ValueName = "measurementMetadata",
            SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, IMeasurementMetadata> Measurements
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                ISequenceSeriesMetadata wrapped = (ISequenceSeriesMetadata)Wrapped;
#else
                ISequenceSeriesMetadata wrapped = Wrapped;
#endif
                if (wrapped.Measurements == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                IDictionary result = new Hashtable();
                foreach (object keysObject in wrapped.Measurements.Keys) {
                    string key = (string)keysObject;
                    IMeasurementMetadata value = (IMeasurementMetadata)wrapped.Measurements[key];
                    result.Add(key, new MeasurementMetadata(value));
                }
                return result;
#else
                return wrapped.Measurements.Select(kvp => new KeyValuePair<string, IMeasurementMetadata>(
                    kvp.Key, (IMeasurementMetadata)new MeasurementMetadata(kvp.Value))).ToDictionary(
                    kvp => kvp.Key, kvp => kvp.Value);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary TextualAttributes
#else
        [JsonProperty("textualAttributes")]
        [YAXSerializeAs("textualAttributes")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "attribute",
            KeyName = "name",
            SerializeKeyAs = YAXNodeTypes.Element,
            ValueName = "value",
            SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, string> TextualAttributes
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceSeriesMetadata)Wrapped).TextualAttributes;
#else
                return Wrapped.TextualAttributes;
#endif
            }
        }

#if MICRO_FRAMEWORK
        public IDictionary ResultColumnSettings
#else
        public IDictionary<string, IList<ResultColumnSetting>> ResultColumnSettings
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceSeriesMetadata)Wrapped).ResultColumnSettings;
#else
                return Wrapped.ResultColumnSettings;
#endif
            }
        }
        #endregion

        #region IMessageBody implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Type
#else
        [JsonProperty("type")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<MessageType>))]
        public MessageType Type
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((ISequenceSeriesMetadata)Wrapped).Type;
#else
                return Wrapped.Type;
#endif
            }
        }
        #endregion

    }
}

