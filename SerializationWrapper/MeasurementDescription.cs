/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System.Linq;
#endif

namespace ProbeNet.Messages.SerializationWrapper
{
    /// <summary>
    /// Wraps a measurement description.
    /// </summary>
    /// <seealso cref="IMeasurementDescription"/>
#if MICRO_FRAMEWORK
    public class MeasurementDescription : Wrapper, IMeasurementDescription
#else
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class MeasurementDescription : Wrapper<IMeasurementDescription>, IMeasurementDescription
#endif
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.SerializationWrapper.MeasurementDescription"/> class.
        /// </summary>
        /// <param name="wrapped">Wrapped.</param>
        public MeasurementDescription(IMeasurementDescription wrapped) :
            base(wrapped)
        {
        }

        #region IMeasurementDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementDescription)Wrapped).Id;
#else
                return Wrapped.Id;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        public string Caption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementDescription)Wrapped).Caption;
#else
                return Wrapped.Caption;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty ("shortCaption")]
        [YAXSerializableField]
        [YAXSerializeAs ("shortCaption")]
        [YAXAttributeForClass]
#endif
        public string ShortCaption
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementDescription)Wrapped).ShortCaption;
#else
                return Wrapped.ShortCaption;
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList TechnicalStandards
#else
        [JsonProperty("technicalStandards")]
        [YAXSerializableField]
        [YAXSerializeAs("technicalStandards")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "technicalStandard")]
        public IList<string> TechnicalStandards
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                return ((IMeasurementDescription)Wrapped).TechnicalStandards;
#else
                return Wrapped.TechnicalStandards;
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("alignmentDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("alignmentDescription")]
#endif
        public IAlignmentDescription AlignmentDescription
        {
            get
            {
#if MICRO_FRAMEWORK
                IMeasurementDescription wrapped = (IMeasurementDescription)Wrapped;
#else
                IMeasurementDescription wrapped = Wrapped;
#endif
                if (wrapped.AlignmentDescription == null) {
                    return null;
                }
                return new AlignmentDescription(wrapped.AlignmentDescription);
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList ParameterDescriptions
#else
        [JsonProperty("parameterDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("parameterDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "parameter")]
        public IList<IParameterDescription> ParameterDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IMeasurementDescription wrapped = (IMeasurementDescription)Wrapped;
#else
                IMeasurementDescription wrapped = Wrapped;
#endif
                if (wrapped.ParameterDescriptions == null) {
                    return wrapped.ParameterDescriptions;
                }
#if MICRO_FRAMEWORK
                return wrapped.ParameterDescriptions;
#else
                return wrapped.ParameterDescriptions.Select(
                    p => (IParameterDescription)new ParameterDescription(p)).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList ResultDescriptions
#else
        [JsonProperty("resultDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("resultDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "result")]
        public IList<IResultDescription> ResultDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IMeasurementDescription wrapped = (IMeasurementDescription)Wrapped;
#else
                IMeasurementDescription wrapped = Wrapped;
#endif
                if (wrapped.ResultDescriptions == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                return wrapped.ParameterDescriptions;
#else
                return Wrapped.ResultDescriptions.Select(
                    r => (IResultDescription)new ResultDescription(r)).ToList();
#endif
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("curveDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("curveDescription")]
#endif
        public ICurveDescription CurveDescription
        {
            get
            {
#if MICRO_FRAMEWORK
                IMeasurementDescription wrapped = (IMeasurementDescription)Wrapped;
#else
                IMeasurementDescription wrapped = Wrapped;
#endif
                if (wrapped.CurveDescription == null) {
                    return null;
                }
                return new CurveDescription(wrapped.CurveDescription);
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList ImageDescriptions
#else
        [JsonProperty("imageDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("imageDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "image")]
        public IList<IImageDescription> ImageDescriptions
#endif
        {
            get
            {
#if MICRO_FRAMEWORK
                IMeasurementDescription wrapped = (IMeasurementDescription)Wrapped;
#else
                IMeasurementDescription wrapped = Wrapped;
#endif
                if (wrapped.ImageDescriptions == null) {
                    return null;
                }
#if MICRO_FRAMEWORK
                return wrapped.ImageDescriptions;
#else
                return Wrapped.ImageDescriptions.Select(
                    i => (IImageDescription)new ImageDescription(i)).ToList();
#endif
            }
        }
        #endregion
    }
}

