#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Cast adapter for dictionaries.
    /// </summary>
	public class DictionaryCastAdapter<TKey, TFrom, TTo>:
		IDictionary<TKey, TTo>
		where TFrom: class
		where TTo: class
	{
		private IDictionary<TKey, TFrom> source;

		private DictionaryCastAdapter (IDictionary<TKey, TFrom> source)
		{
			this.source = source;
		}
        
        /// <summary>
        /// Build the specified source.
        /// </summary>
        /// <param name="source">Source.</param>
        public static DictionaryCastAdapter<TKey, TFrom, TTo> Build(IDictionary<TKey, TFrom> source)
        {
            if(source == null) {
                return null;
            }
            return new DictionaryCastAdapter<TKey, TFrom, TTo>(source);
        }

		#region IDictionary[TKey,TTo] implementation
        /// <inheritdoc/>
		public void Add (TKey key, TTo value)
		{
			source.Add (key, value as TFrom);
		}

        /// <inheritdoc/>
		public bool ContainsKey (TKey key)
		{
			return source.ContainsKey(key);
		}

        /// <inheritdoc/>
		public bool Remove (TKey key)
		{
			return source.Remove (key);
		}

        /// <inheritdoc/>
		public bool TryGetValue (TKey key, out TTo value)
		{
			TFrom f;
			bool success = source.TryGetValue(key, out f);
			value = f as TTo;
			return success;
		}

        /// <inheritdoc/>
		public TTo this[TKey key] {
			get {
				return source[key] as TTo;
			}
			set {
				source[key] = value as TFrom;
			}
		}

        /// <inheritdoc/>
		public ICollection<TKey> Keys {
			get {
				return source.Keys;
			}
		}

        /// <inheritdoc/>
		public ICollection<TTo> Values {
			get {
				return new CollectionCastAdapter<TFrom, TTo>(source.Values);
			}
		}
		#endregion

		#region IEnumerable[System.Collections.Generic.KeyValuePair[TKey,TTo]] implementation
        /// <inheritdoc/>
		public IEnumerator<KeyValuePair<TKey, TTo>> GetEnumerator ()
		{
			return new DictionaryCastEnumerator<TKey, TFrom, TTo>(source);
		}
		#endregion

		#region IEnumerable implementation
        /// <inheritdoc/>
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (source as IEnumerable).GetEnumerator();
		}
		#endregion
	
		#region ICollection[System.Collections.Generic.KeyValuePair[TKey,TTo]] implementation
        /// <inheritdoc/>
		public void Add (KeyValuePair<TKey, TTo> item)
		{
			source.Add (new KeyValuePair<TKey, TFrom>(item.Key, item.Value as TFrom));
		}
	
        /// <inheritdoc/>
		public void Clear ()
		{
			source.Clear ();
		}

        /// <inheritdoc/>
		public bool Contains (KeyValuePair<TKey, TTo> item)
		{
			return source.Contains (new KeyValuePair<TKey, TFrom>(item.Key, item.Value as TFrom));
		}

        /// <inheritdoc/>
		public void CopyTo (KeyValuePair<TKey, TTo>[] array, int arrayIndex)
		{
			throw new NotImplementedException ();
		}

        /// <inheritdoc/>
		public bool Remove (KeyValuePair<TKey, TTo> item)
		{
			return source.Remove (new KeyValuePair<TKey, TFrom>(item.Key, item.Value as TFrom));
		}

        /// <inheritdoc/>
		public int Count {
			get {
				return source.Count;
			}
		}

        /// <inheritdoc/>
		public bool IsReadOnly {
			get {
				return source.IsReadOnly;
			}
		}
		#endregion
	}
}
#endif
