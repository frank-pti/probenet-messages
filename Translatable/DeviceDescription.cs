#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Internationalization;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class DeviceDescription: Base.Generic.DeviceDescription<MeasurementDescription, SequenceDescription>, IDeviceDescription
    {
        private TranslationString caption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.DeviceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
        [JsonConstructor]
        public DeviceDescription (string id, Guid uuid):
            base(id, uuid)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.DeviceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
        /// <param name="caption">Caption.</param>
        public DeviceDescription (string id, Guid uuid, TranslationString caption):
            base(id, uuid)
        {
            Caption = caption;
        }

#region implemented abstract members of ProbeNet.Messages.Base.DeviceDescription
        /// <inheritdoc/>
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
        protected override string CaptionString {
            get {
                return Caption.Tr;
            }
        }
        #endregion

        /// <inheritdoc/>
        [YAXDontSerialize]
        [JsonIgnore]
        public virtual TranslationString Caption {
            get {
                return caption;
            }
            set {
                if (value == null) {
                    throw new ArgumentNullException ();
                }
                caption = value;
            }
        }

#region IDeviceDescription implementation
        System.Collections.Generic.IList<ISequenceDescription> IDeviceDescription.Sequences {
            get {
                return ListCastAdapter<SequenceDescription, ISequenceDescription>.Build(Sequences);
            }
        }

        System.Collections.Generic.IList<IMeasurementDescription> IDeviceDescription.Measurements {
            get {
                return ListCastAdapter<MeasurementDescription, IMeasurementDescription>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif