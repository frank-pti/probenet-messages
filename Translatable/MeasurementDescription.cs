#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Internationalization;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class MeasurementDescription:
        Base.Generic.MeasurementDescription<AlignmentDescription, ParameterDescription, ResultDescription, CurveDescription, ImageDescription>, IMeasurementDescription
    {
        private TranslationString caption;
        private TranslationString shortCaption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.MeasurementDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="shortCaption">Short caption.</param>
        public MeasurementDescription (string id, TranslationString caption, TranslationString shortCaption):
            base(id)
        {
            Caption = caption;
            ShortCaption = shortCaption;
        }

#region implemented abstract members of ProbeNet.Messages.Base.MeasurementDescription
        /// <inheritdoc/>
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
        protected override string CaptionString {
            get {
                return Caption.Tr;
            }
        }

        /// <inheritdoc/>
        [JsonProperty("shortCaption")]
        [YAXSerializableField]
        [YAXSerializeAs("shortCaption")]
        [YAXAttributeForClass]
        protected override string ShortCaptionString {
            get {
                return ShortCaption.Tr;
            }
        }
        #endregion

        /// <inheritdoc/>
        [YAXDontSerialize]
        public TranslationString Caption {
            get {
                return caption;
            }
            set {
                if (value == null) {
                    throw new ArgumentNullException ();
                }
                caption = value;
            }
        }

        /// <inheritdoc/>
        [YAXDontSerialize]
        public TranslationString ShortCaption {
            get {
                return shortCaption;
            }
            set {
                if (value == null) {
                    throw new ArgumentException();
                }
                shortCaption = value;
            }
        }

        /// <inheritdoc/>
        [JsonProperty("alignmentDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("alignmentDescription")]
        public override AlignmentDescription AlignmentDescription {
            get {
                return base.AlignmentDescription;
            }
            set {
                base.AlignmentDescription = value;
            }
        }

        /// <inheritdoc/>
        [JsonProperty("curveDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("curveDescription")]
        public override ProbeNet.Messages.Translatable.CurveDescription CurveDescription {
            get {
                return base.CurveDescription;
            }
            set {
                base.CurveDescription = value;
            }
        }

#region IMeasurementDescription implementation
        IList<IParameterDescription> IMeasurementDescription.ParameterDescriptions {
            get {
                return ListCastAdapter<ParameterDescription, IParameterDescription>.Build(ParameterDescriptions);
            }
        }

        IList<IResultDescription> IMeasurementDescription.ResultDescriptions {
            get {
                return ListCastAdapter<ResultDescription, IResultDescription>.Build(ResultDescriptions);
            }
        }

        IAlignmentDescription IMeasurementDescription.AlignmentDescription {
            get {
                return AlignmentDescription;
            }
        }

        ICurveDescription IMeasurementDescription.CurveDescription {
            get {
                return CurveDescription;
            }
        }
        #endregion
    }
}
#endif