#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using ProbeNet.Enums;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class CurveDescription: Base.Generic.CurveDescription<AxisDescription>, ICurveDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.CurveDescription"/> class.
        /// </summary>
        public CurveDescription ():
            base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.CurveDescription"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        public CurveDescription (CoordinateSystemType coordinateSystem):
            base(coordinateSystem)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.CurveDescription"/> class.
        /// </summary>
        /// <param name="axes">Axes.</param>
        public CurveDescription(IList<AxisDescription> axes):
            base(axes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.CurveDescription"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        /// <param name="axes">Axes.</param>
        public CurveDescription (CoordinateSystemType coordinateSystem, IList<AxisDescription> axes):
            base(coordinateSystem, axes)
        {
        }

        /// <inheritdoc/>
        [JsonProperty("axes")]
        [YAXSerializableField]
        [YAXSerializeAs("axes")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "axisDescription")]
        public override IList<AxisDescription> Axes {
            get {
                return base.Axes;
            }
            set {
                base.Axes = value;
            }
        }

#region ICurveDescription implementation
        IList<IAxisDescription> ICurveDescription.Axes {
            get {
                return ListCastAdapter<AxisDescription, IAxisDescription>.Build(Axes);
            }
        }
        #endregion
    }
}
#endif