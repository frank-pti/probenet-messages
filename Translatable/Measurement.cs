#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class Measurement: Base.Measurement, ProbeNet.Messages.Translatable.Interface.IMeasurement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Measurement"/> class.
        /// </summary>
        public Measurement ():
            this(Guid.NewGuid())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Measurement"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        public Measurement(Guid uuid):
            base(uuid)
        {
            Results = new Dictionary<string, Nullable<double>>();
        }

        /// <inheritdoc/>
        [JsonProperty("alignment", NullValueHandling = NullValueHandling.Ignore)]
        [YAXSerializableField]
        [YAXSerializeAs("alignment")]
        public Alignment Alignment {
            get;
            set;
        }

        /// <inheritdoc/>
        protected override ProbeNet.Messages.Base.Alignment BaseAlignment {
            get {
                return Alignment;
            }
        }

#region IMeasurement implementation
        IAlignment IMeasurement.Alignment {
            get {
                return Alignment;
            }
        }
        #endregion
    }
}
#endif