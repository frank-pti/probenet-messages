#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class Sequence: Base.Generic.Sequence<Measurement>, ISequence
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Sequence"/> class.
        /// </summary>
        public Sequence():
            this(Guid.NewGuid(), DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        public Sequence(Guid uuid):
            this(uuid, DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Sequence"/> class.
        /// </summary>
        /// <param name="moment">Moment.</param>
        public Sequence(DateTime moment):
            this(Guid.NewGuid(), moment)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        /// <param name="moment">Moment.</param>
        public Sequence(Guid uuid, DateTime moment):
            base(uuid, moment)
        {
        }

#region ISequence implementation
        IDictionary<string, IMeasurement> ISequence.Measurements {
            get {
                return DictionaryCastAdapter<string, Measurement, IMeasurement>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif