#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Internationalization;
using ProbeNet.Enums;
using YAXLib;
using Newtonsoft.Json;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class ResultDescription: Base.ResultDescription, IResultDescription
    {
        private TranslationString caption;
        private TranslationString unit;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.ResultDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="source">Source.</param>
        public ResultDescription (string id, TranslationString caption, SourceType source):
            this(id, caption, caption.I18n.TrObject(caption.Domain, ""), source)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.ResultDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        /// <param name="unit">Unit.</param>
        /// <param name="source">Source.</param>
        public ResultDescription (string id, TranslationString caption, TranslationString unit, SourceType source):
            base(id, source)
        {
            Caption = caption;
            Unit = unit;
        }

#region implemented abstract members of ProbeNet.Messages.Base.ResultDescription
        /// <inheritdoc/>
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
        protected override string CaptionString {
            get {
                return Caption.Tr;
            }
        }

        /// <inheritdoc/>
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
        protected override string UnitString {
            get {
                return Unit.Tr;
            }
        }
        #endregion

        /// <inheritdoc/>
        [YAXDontSerialize]
        public TranslationString Caption {
            get {
                return caption;
            }
            set {
                if (value == null) {
                    throw new ArgumentNullException ();
                }
                caption = value;
            }
        }

        /// <inheritdoc/>
        [YAXDontSerialize]
        public TranslationString Unit {
            get {
                return unit;
            }
            set {
                if (value == null) {
                    throw new ArgumentNullException ();
                }
                unit = value;
            }
        }
    }
}
#endif