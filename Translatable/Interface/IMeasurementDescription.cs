#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable.Interface
{
    /// <inheritdoc/>
    public interface IMeasurementDescription: ProbeNet.Messages.Interface.IMeasurementDescription
    {
        /// <summary>
        /// Gets the translatable caption.
        /// </summary>
        /// <value>A human-readable name of the measurement in the language in which it is sent.</value>
        new TranslationString Caption {
            get;
        }

        /// <summary>
        /// Gets the translatable short caption.
        /// </summary>
        /// <value>A human-readable short name of the measurement in the language in which it is sent.
        /// Maximum length: 10 characters.</value>
        new TranslationString ShortCaption {
            get;
        }

        /// <inheritdoc/>
        new IList<IParameterDescription> ParameterDescriptions {
            get;
        }

        /// <inheritdoc/>
        new IList<IResultDescription> ResultDescriptions {
            get;
        }

        /// <inheritdoc/>
        new IAlignmentDescription AlignmentDescription {
            get;
        }

        /// <inheritdoc/>
        new ICurveDescription CurveDescription {
            get;
        }
    }
}
#endif
