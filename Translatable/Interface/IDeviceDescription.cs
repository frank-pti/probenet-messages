#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable.Interface
{
    /// <inheritdoc/>
    public interface IDeviceDescription: ProbeNet.Messages.Interface.IDeviceDescription, IMessageBody
    {
        /// <summary>
        /// Gets the translatable caption.
        /// </summary>
        /// <value>A human-readable name of the device in the language in which the message is sent.</value>
        new TranslationString Caption {
            get;
        }

        /// <inheritdoc/>
        new IList<ISequenceDescription> Sequences {
            get;
        }

        /// <inheritdoc/>
        new IList<IMeasurementDescription> Measurements {
            get;
        }
    }
}
#endif
