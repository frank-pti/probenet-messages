#if !MICRO_FRAMEWORK
/*
 * Base project for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Internationalization;

namespace ProbeNet.Messages.Translatable.Interface
{
    /// <summary>
    /// Interface for translatable value description.
    /// </summary>
    public interface ITranslatableValueDescription
    {
        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
        TranslationString Caption
        {
            get;
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>The unit.</value>
        TranslationString Unit
        {
            get;
        }
    }
}
#endif
