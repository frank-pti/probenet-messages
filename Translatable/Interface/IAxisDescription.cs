#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace ProbeNet.Messages.Translatable.Interface
{
    /// <inheritdoc/>
    public interface IAxisDescription: ProbeNet.Messages.Interface.IAxisDescription, ITranslatableValueDescription
    {
        /// <summary>
        /// Gets the translatable caption.
        /// </summary>
        /// <value>A human-readable name of the axis in the language in which it is sent.</value>
        new TranslationString Caption {
            get;
        }

        /// <summary>
        /// Gets the translatable unit.
        /// </summary>
        /// <value>The unit of the axis. May be omitted if the axis does not have a unit.</value>
        new TranslationString Unit {
            get;
        }
    }
}
#endif

