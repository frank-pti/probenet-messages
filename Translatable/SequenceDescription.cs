#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Newtonsoft.Json;
using YAXLib;
using ProbeNet.Messages.Translatable.Interface;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class SequenceDescription: Base.Generic.SequenceDescription<SequenceDescriptionMeasurementReference>, ISequenceDescription
    {
        private TranslationString caption;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.SequenceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="caption">Caption.</param>
        public SequenceDescription (string id, TranslationString caption):
            base(id)
        {
            Caption = caption;
        }

#region implemented abstract members of ProbeNet.Messages.Base.SequenceDescription
        /// <inheritdoc/>
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
        protected override string CaptionString {
            get {
                return Caption.Tr;
            }
        }
        #endregion

        /// <inheritdoc/>
        [YAXDontSerialize]
        public virtual TranslationString Caption {
            get {
                return caption;
            }
            set {
                if (value == null) {
                    throw new ArgumentNullException ();
                }
                caption = value;
            }
        }

#region ISequenceDescription implementation
        IList<ISequenceDescriptionMeasurementReference> ISequenceDescription.Measurements {
            get {
                return ListCastAdapter<SequenceDescriptionMeasurementReference, ISequenceDescriptionMeasurementReference>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif