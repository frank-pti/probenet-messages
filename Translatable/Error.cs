#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Internationalization;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class Error: Base.Error, IError
    {
        private TranslationString message;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.Error"/> class.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="message">Message.</param>
        public Error(ErrorType type, TranslationString message):
            base(type)
        {
            Message = message;
        }

#region implemented abstract members of ProbeNet.Messages.Base.Error
        /// <inheritdoc/>
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
        protected override string MessageString {
            get {
                return Message.Tr;
            }
        }
        #endregion

        /// <inheritdoc/>
        [YAXDontSerialize]
        public TranslationString Message {
            get {
                return message;
            }
            set {
                if (value == null)
                    throw new ArgumentNullException ();
                message = value;
            }
        }
    }
}
#endif