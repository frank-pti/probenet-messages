#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Translatable.Interface;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Translatable
{
    /// <inheritdoc/>
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public class SequenceSeriesMetadata: Base.Generic.SequenceSeriesMetadata<MeasurementMetadata>, ISequenceSeriesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Translatable.SequenceSeriesMetadata"/> class.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        /// <param name="uuid">UUID.</param>
        public SequenceSeriesMetadata(string sequence, Guid uuid):
            base(sequence, uuid)
        {
        }

#region ISequenceSeriesMetadata implementation
        IDictionary<string, IMeasurementMetadata> ISequenceSeriesMetadata.Measurements {
            get {
                return DictionaryCastAdapter<string, MeasurementMetadata, IMeasurementMetadata>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif