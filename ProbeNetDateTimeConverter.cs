#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Converter for ProbeNet Date/Time data.
    /// </summary>
	public class ProbeNetDateTimeConverter: DateTimeConverterBase
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.ProbeNetDateTimeConverter"/> class.
        /// </summary>
		public ProbeNetDateTimeConverter ()
		{
		}

        /// <inheritdoc/>
		public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			DateTime moment;
            if (reader.TokenType == JsonToken.String)
            {
                moment = DateTime.Parse((string)reader.Value);
            }
            else if (reader.TokenType == JsonToken.Date)
            {
                moment = (DateTime)reader.Value;
            }
            else
			{
				throw new Exception(String.Format ("Date must be a string or a Date. Got {0}", reader.TokenType));
			}
			return moment;
		}

        /// <inheritdoc/>
		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value is DateTime) {
				DateTime moment = ((DateTime)(value)).ToUniversalTime();
                string formatted = String.Format(
                    "{0:0000}-{1:00}-{2:00}T{3:00}:{4:00}:{5:00}.{6:000}+00:00",
                    moment.Year, moment.Month, moment.Day,
                    moment.Hour, moment.Minute, moment.Second, moment.Millisecond);
                writer.WriteValue(formatted);
			} else {
				throw new Exception("Expected date object value");
			}
		}
	}
}
#endif
