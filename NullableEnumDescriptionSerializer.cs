#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using YAXLib;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Nullable enum description serializer.
    /// </summary>
    public class NullableEnumDescriptionSerializer<T>: ICustomSerializer<Nullable<T>>
        where T: struct
    {
        private EnumDescriptionSerializer<T> enumDescriptionSerializer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.NullableEnumDescriptionSerializer`1"/> class.
        /// </summary>
        public NullableEnumDescriptionSerializer ()
        {
            enumDescriptionSerializer = new EnumDescriptionSerializer<T>();
        }

#region ICustomSerializer implementation
        /// <inheritdoc/>
        public void SerializeToAttribute (Nullable<T> objectToSerialize, System.Xml.Linq.XAttribute attrToFill)
        {
            if (objectToSerialize != null) {
                enumDescriptionSerializer.SerializeToAttribute(objectToSerialize.Value, attrToFill);
            }
        }

        /// <inheritdoc/>
        public void SerializeToElement (Nullable<T> objectToSerialize, System.Xml.Linq.XElement elemToFill)
        {
            if (objectToSerialize != null) {
                enumDescriptionSerializer.SerializeToElement(objectToSerialize.Value, elemToFill);
            }
        }

        /// <inheritdoc/>
        public string SerializeToValue (Nullable<T> objectToSerialize)
        {
            if (objectToSerialize != null) {
                return enumDescriptionSerializer.SerializeToValue(objectToSerialize.Value);
            }
            return String.Empty;
        }

        /// <inheritdoc/>
        public Nullable<T> DeserializeFromAttribute (System.Xml.Linq.XAttribute attrib)
        {
            return enumDescriptionSerializer.DeserializeFromAttribute(attrib);
        }

        /// <inheritdoc/>
        public Nullable<T> DeserializeFromElement (System.Xml.Linq.XElement element)
        {
            return enumDescriptionSerializer.DeserializeFromElement(element);
        }

        /// <inheritdoc/>
        public Nullable<T> DeserializeFromValue (string value)
        {
            return enumDescriptionSerializer.DeserializeFromValue(value);
        }
        #endregion
    }
}
#endif
