#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Adapter for casting collections.
    /// </summary>
	public class CollectionCastAdapter<TFrom, TTo>: ICollection<TTo>
		where TFrom: class
		where TTo: class
	{
		private ICollection<TFrom> source;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.CollectionCastAdapter`2"/> class.
        /// </summary>
        /// <param name="source">Source.</param>
		public CollectionCastAdapter (ICollection<TFrom> source)
		{
			this.source = source;
		}

#region ICollection[TTo] implementation
        /// <inheritdoc/>
		public void Add (TTo item)
		{
			source.Add (item as TFrom);
		}
        
        /// <inheritdoc/>
		public void Clear ()
		{
			source.Clear ();
		}

        /// <inheritdoc/>
		public bool Contains (TTo item)
		{
			return source.Contains (item as TFrom);
		}

        /// <inheritdoc/>
		public void CopyTo (TTo[] array, int arrayIndex)
		{
			source.CopyTo (array as TFrom[], arrayIndex);
		}

        /// <inheritdoc/>
		public bool Remove (TTo item)
		{
			return source.Remove (item as TFrom);
		}

        /// <inheritdoc/>
		public int Count {
			get {
				return source.Count;
			}
		}

        /// <inheritdoc/>
		public bool IsReadOnly {
			get {
				return source.IsReadOnly;
			}
		}
        #endregion

#region IEnumerable[TTo] implementation
        /// <inheritdoc/>
		public IEnumerator<TTo> GetEnumerator ()
		{
			return new EnumeratorCastAdapter<TFrom, TTo>(source.GetEnumerator ());
		}
        #endregion

#region IEnumerable implementation
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (source as IEnumerable).GetEnumerator ();
		}
        #endregion
	}
}
#endif
