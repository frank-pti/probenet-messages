#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Adapter for casting Lists.
    /// </summary>
	public class ListCastAdapter<TFrom, TTo>: IList<TTo>
		where TTo: class
		where TFrom: class
	{
		private IList<TFrom> source;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.ListCastAdapter`2"/> class.
        /// </summary>
        /// <param name="source">Source.</param>
		private ListCastAdapter (IList<TFrom> source)
		{
			this.source = source;
		}

        /// <summary>
        /// Build the specified source.
        /// </summary>
        /// <param name="source">Source.</param>
        public static ListCastAdapter<TFrom, TTo> Build(IList<TFrom> source)
        {
            if (source == null) {
                return null;
            }
            return new ListCastAdapter<TFrom, TTo>(source);
        }

#region IList[To] implementation
        /// <inheritdoc/>
		public int IndexOf (TTo item)
		{
			return source.IndexOf(item as TFrom);
		}

        /// <inheritdoc/>
		public void Insert (int index, TTo item)
		{
			source.Insert (index, item as TFrom);
		}

        /// <inheritdoc/>
		public void RemoveAt (int index)
		{
			source.RemoveAt (index);
		}

        /// <inheritdoc/>
		public TTo this[int index] {
			get {
				return source[index] as TTo;
			}
			set {
				source[index] = value as TFrom;
			}
		}
        #endregion

#region IEnumerable[To] implementation
        /// <inheritdoc/>
		public IEnumerator<TTo> GetEnumerator ()
		{
			return new EnumeratorCastAdapter<TFrom, TTo>(source.GetEnumerator());
		}
        #endregion
	
#region IEnumerable implementation
        /// <inheritdoc/>
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (source as IEnumerable).GetEnumerator();
		}
        #endregion
	
#region ICollection[To] implementation
        /// <inheritdoc/>
		public void Add (TTo item)
		{
			source.Add (item as TFrom);
		}

        /// <inheritdoc/>
		public void Clear ()
		{
			source.Clear ();
		}

        /// <inheritdoc/>
		public bool Contains (TTo item)
		{
			return source.Contains (item as TFrom);
		}

        /// <inheritdoc/>
		public void CopyTo (TTo[] array, int arrayIndex)
		{
			source.CopyTo (array as TFrom[], arrayIndex);
		}

        /// <inheritdoc/>
		public bool Remove (TTo item)
		{
			return source.Remove (item as TFrom);
		}

        /// <inheritdoc/>
		public int Count {
			get {
				return source.Count;
			}
		}

        /// <inheritdoc/>
		public bool IsReadOnly {
			get {
				return source.IsReadOnly;
			}
		}
        #endregion
	}
}
#endif
