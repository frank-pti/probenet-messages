/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if !MICRO_FRAMEWORK
using YAXLib;
#endif

namespace ProbeNet.Messages.Wrapper
{
    /// <summary>
    /// Wrapps a point of a curve for XML output using Yaxlib.
    /// </summary>
#if !MICRO_FRAMEWORK
    [YAXSerializeAs("measurement")]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
#endif
    public class PointWrapper
    {
        private double[] point;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Wrapper.PointWrapper"/> class.
        /// </summary>
        /// <param name="point">Point.</param>
        public PointWrapper(double[] point)
        {
            this.point = point;
        }

        /// <summary>
        /// X coordinate.
        /// </summary>
        /// <value>X</value>
#if !MICRO_FRAMEWORK
        [YAXSerializableField]
        [YAXSerializeAs("x")]
        [YAXAttributeForClass]
#endif
        public double X
        {
            get
            {
                if (point.Length < 1) {
                    return double.NaN;
                }
                return point[0];
            }
        }

        /// <summary>
        /// Y coordinate
        /// </summary>
        /// <value>Y</value>
#if !MICRO_FRAMEWORK
        [YAXSerializableField]
        [YAXSerializeAs("y")]
        [YAXAttributeForClass]
#endif
        public double Y
        {
            get
            {
                if (point.Length < 2) {
                    return double.NaN;
                }
                return point[1];
            }
        }
    }
}

