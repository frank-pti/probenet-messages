/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Wrapper
{
    /// <summary>
    /// Wraps a curve which is a list of points for XML output using Yaxlib.
    /// </summary>
    /// <seealso cref="PointWrapper"/>
    public class CurveWrapper :
#if MICRO_FRAMEWORK
 ArrayList
#else
        List<PointWrapper>
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Wrapper.CurveWrapper"/> class.
        /// </summary>
        /// <param name="curve">Curve.</param>
        public CurveWrapper(
#if MICRO_FRAMEWORK
IList curve
#else
            IList<double[]> curve
#endif
)
        {
            if (curve != null) {
#if MICRO_FRAMEWORK
                foreach (object p in curve) {
                    double[] point = (double[])p;
#else
                foreach (double[] point in curve) {
#endif
                    Add(new PointWrapper(point));
                }
            }
        }
    }
}

