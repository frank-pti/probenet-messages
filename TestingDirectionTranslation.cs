#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Helper class for getting translatable strings for the testing direction.
    /// </summary>
	public static class TestingDirectionTranslation
	{
        /// <summary>
        /// Gets the direction translation.
        /// </summary>
        /// <returns>The direction translation.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="direction">Direction.</param>
		public static TranslationString GetDirectionTranslation(I18n i18n, int direction)
		{
            Initializer.Initialize(i18n);
			if (direction == 0) {
				return i18n.TrObject (Constants.ProbeNetMessagesDomain, "Machine direction");
			} else if (direction == 90) {
				return i18n.TrObject (Constants.ProbeNetMessagesDomain, "Cross direction");
			} else {
				return i18n.TrObject (Constants.ProbeNetMessagesDomain, "{0}°", direction);
			}
		}

        /// <summary>
        /// Gets the direction short translation.
        /// </summary>
        /// <returns>The direction short translation.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="direction">Direction.</param>
        public static TranslationString GetDirectionShortTranslation(I18n i18n, int direction)
        {
            Initializer.Initialize(i18n);
            if (direction == 0) {
                return i18n.TrObject (Constants.ProbeNetMessagesDomain, "Mach");
            } else if (direction == 90) {
                return i18n.TrObject (Constants.ProbeNetMessagesDomain, "Cross");
            } else {
                return i18n.TrObject (Constants.ProbeNetMessagesDomain, "{0}°", direction);
            }
        }
	}
}
#endif
