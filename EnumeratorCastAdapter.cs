#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Cast adapter for enumerator.
    /// </summary>
	public class EnumeratorCastAdapter<TFrom, TTo>: IEnumerator<TTo>
		where TFrom: class
		where TTo: class
	{
		private IEnumerator<TFrom> source;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.EnumeratorCastAdapter`2"/> class.
        /// </summary>
        /// <param name="source">Source.</param>
		public EnumeratorCastAdapter (IEnumerator<TFrom> source)
		{
			this.source = source;
		}

#region IEnumerator[To] implementation
        /// <inheritdoc/>
		public TTo Current {
			get {
				return source.Current as TTo;
			}
		}
        #endregion

#region IEnumerator implementation
        /// <inheritdoc/>
		public bool MoveNext ()
		{
			return source.MoveNext();
		}

        /// <inheritdoc/>
		public void Reset ()
		{
			source.Reset ();
		}

		object IEnumerator.Current {
			get {
				return (source as IEnumerator).Current;
			}
		}
        #endregion

#region IDisposable implementation
        /// <inheritdoc/>
		public void Dispose ()
		{
			source.Dispose ();
		}
        #endregion
	}
}
#endif
