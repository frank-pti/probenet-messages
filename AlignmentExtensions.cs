#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Class for extending the <see cref="IAlignment"/> interface.
    /// </summary>
    public static class AlignmentExtensions
    {
        /// <summary>
        /// Gets the translatable label.
        /// </summary>
        /// <returns>The label.</returns>
        /// <param name="alignment">Alignment.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="defaultLabel">The default label that should be used if alignment has neither side nor direction.</param>
        public static TranslationString GetLabel (this IAlignment alignment, I18n i18n, TranslationString defaultLabel)
        {
            return alignment.GetLabel (i18n, defaultLabel, null);
        }

        /// <summary>
        /// Gets the translatable label.
        /// </summary>
        /// <returns>The label.</returns>
        /// <param name="alignment">Alignment.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="defaultLabel">The default label that should be used if alignment has neither side nor direction.</param>
        /// <param name="prefix">A prefix that should be inserted at the beginning.</param>
        public static TranslationString GetLabel (this IAlignment alignment, I18n i18n, TranslationString defaultLabel, TranslationString prefix)
        {
            TranslationString direction = null;
            if (alignment.Direction != null) {
                direction = TestingDirectionTranslation.GetDirectionTranslation (i18n, alignment.Direction.Value);
            }
            return alignment.GetLabel(i18n, defaultLabel, prefix, direction);
        }

        /// <summary>
        /// Gets the translatable short label.
        /// </summary>
        /// <returns>The short label.</returns>
        /// <param name="alignment">Alignment.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="defaultLabel">The default label that should be used if alignment has neither side nor direction.</param>
        /// <param name="prefix">A prefix that should be inserted at the beginning.</param>
        public static TranslationString GetShortLabel(this IAlignment alignment, I18n i18n, TranslationString defaultLabel, TranslationString prefix)
        {
            TranslationString direction = null;
            if (alignment.Direction != null) {
                direction = TestingDirectionTranslation.GetDirectionShortTranslation (i18n, alignment.Direction.Value);
            }
            return alignment.GetLabel(i18n, defaultLabel, prefix, direction);
        }

        private static TranslationString GetLabel (this IAlignment alignment, I18n i18n, TranslationString defaultLabel, TranslationString prefix, TranslationString direction)
        {
            Initializer.Initialize (i18n);
            TranslationString side = null;

            if (alignment.Side != null) {
                side = alignment.Side.Value.GetName (i18n);
            }

            if (prefix != null) {
                prefix = i18n.TrObject (Constants.ProbeNetMessagesDomain, "{0} ", prefix);
            }

            if (side != null && direction != null) {
                return i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0}{1} {2}", prefix, direction, side);
            }
            if (side != null) {
                return i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0}{1}", prefix, side);
            }
            if (direction != null) {
                return i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0}{1}", prefix, direction);
            }
            return defaultLabel;
        }

        /// <summary>
        /// Gets the name of the side as translatable string
        /// </summary>
        /// <returns>The name.</returns>
        /// <param name="side">Side.</param>
        /// <param name="i18n">I18n.</param>
        public static TranslationString GetName(this SampleSide side, I18n i18n)
        {
            switch(side) {
            case SampleSide.Top:
                return i18n.TrObject(Constants.ProbeNetMessagesDomain, "Top");
            case SampleSide.Bottom:
                return i18n.TrObject(Constants.ProbeNetMessagesDomain, "Bottom");
            }
            throw new ArgumentException(String.Format ("Invalid side: {0}", side));
        }
    }
}
#endif
