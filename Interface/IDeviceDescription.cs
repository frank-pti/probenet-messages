/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif


namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Device description message
    /// </summary>
    /// <description>
    /// The device description tells some general detail about the device, and which sequences it can execute.
    /// </description>
    public interface IDeviceDescription : IMessageBody
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// An identification string for the model. This is the same for all products of the same model. It can be
        /// used to reference the type of device.
        /// </value>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// A human-readable name of the device in the language in which the message is sent.
        /// </value>
        string Caption
        {
            get;
        }

        /// <summary>
        /// Gets the ProbeNet protocol version.
        /// </summary>
        /// <value>
        /// The ProbeNet protocol version.
        /// </value>
        int ProbeNetVersion
        {
            get;
        }

        /// <summary>
        /// Gets the model number.
        /// </summary>
        /// <value>
        /// The model number according to the manufacturer's principles.
        /// </value>
        string ModelNumber
        {
            get;
        }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        /// <value>
        /// The serial number of the device, as it is assigned by the manufacturer.
        /// </value>
        string SerialNumber
        {
            get;
        }

        /// <summary>
        /// Gets the firmware version.
        /// </summary>
        /// <value>
        /// The firmware version. This is a free-text string field, which is readable to humans, but is not intended
        /// to be processed by computers.
        /// </value>
        string FirmwareVersion
        {
            get;
        }

        /// <summary>
        /// Gets the manufacturer.
        /// </summary>
        /// <value>
        /// The manufacturer. This is a free-text string field, which is readable to humans, but is not intended to
        /// be processed by computers.
        /// </value>
        string Manufacturer
        {
            get;
        }

        /// <summary>
        /// Gets the extensions.
        /// </summary>
        /// <value>
        /// The list of ProbeNet Protocol extension ids that are supported by the measurement device. The id can be
        /// found in the specification of the extension.
        /// </value>
#if MICRO_FRAMEWORK
        IList Extensions
#else
        IList<string> Extensions
#endif
        {
            get;
        }


#if MICRO_FRAMEWORK
        /// <summary>
        /// Gets the count of sequences.
        /// </summary>
        /// <value>
        /// The amount of sequences which can be executed by the measurement device. The sequences objects must be
        /// requested separately. The structure of the object is described in the <see cref="ISequenceDescription"/>
        /// section.</value>
        int SequenceCount
#else
        /// <summary>
        /// Gets the sequences.
        /// </summary>
        /// <value>
        /// The sequences which can be executed by the measurement device. The structure of the object is described
        /// in the <see cref="ISequenceDescription"/> section.
        /// </value>
        /// <seealso cref="ISequenceDescription"/>
		IList<ISequenceDescription> Sequences 
#endif
        {
            get;
        }


#if MICRO_FRAMEWORK
        /// <summary>
        /// Gets the count of measurements.
        /// </summary>
        /// <value>The amount of measurements which can be sent by the measurement device. The measurement objects must be
        /// requested separately. The structure of the object is described in the <see cref="IMeasurementDescription"/>
        /// object.</value>
        int MeasurementCount
#else
        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>
        /// The measurements which can be sent by the measurement device. The structure of the object is described
        /// in the <see cref="IMeasurementDescription"/> section.
        /// </value>
        /// <seealso cref="IMeasurementDescription"/>
		IList<IMeasurementDescription> Measurements
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the installed languages.
        /// </summary>
        /// <value>
        /// The languages which are installed on the measurement device and can accordingly be used in the messages.
        /// The strings are the same two-letter codes as used in the message header.
        /// </value>
#if MICRO_FRAMEWORK
        IList InstalledLanguages
#else
		IList<string> InstalledLanguages
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the icon.
        /// </summary>
        /// <value>
        /// The icon of the measurement device as base-64-encoded PNG file. It must be 64 pixels wide and 64 pixels
        /// high. It should usually be a pictograph of the measurement device. The icon can be displayed on computer
        /// screens to allow easy visual association for people who work with several measurement devices.
        /// </value>
        byte[] Icon
        {
            get;
        }

        /// <summary>
        /// Gets the UUID.
        /// </summary>
        /// <value>
        /// A universally unique identifier according to <a href="http://tools.ietf.org/html/rfc4122">RFC 4122</a>.
        /// This identifies a single device, not the model. So it is different for each device, no matter if they are
        /// of the same model.
        /// </value>
        Guid Uuid
        {
            get;
        }
    }
}