/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Enums;

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Result description.
    /// </summary>
	public interface IResultDescription
	{
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier of the result. It must be unique within the measurement description.
        /// </value>
		string Id {
			get;
		}
		
        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// A human-readable name of the result in the language in which it is sent.
        /// </value>
		string Caption {
			get;
		}
		
        /// <summary>
        /// Gets the source.
        /// </summary>
        /// <value>
        /// The source of the result.
        /// </value>
        /// <seealso cref="SourceType"/>
#if MICRO_FRAMEWORK
        string Source
#else
		SourceType Source
#endif
        {
			get;
		}
		
        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit of the result. May be ommited if the result does not have a unit.
        /// </value>
		string Unit {
			get;
		}
		
        /// <summary>
        /// Gets the minimum.
        /// </summary>
        /// <value>
        /// The lowest value that is allowed for the result.
        /// </value>
		double Minimum {
			get;
		}
		
        /// <summary>
        /// Gets the maximum.
        /// </summary>
        /// <value>
        /// The highest value that is allowed for the result.
        /// </value>
		double Maximum {
			get;
		}
		
        /// <summary>
        /// Gets the decimal places.
        /// </summary>
        /// <value>
        /// The decimal places.
        /// </value>
		int DecimalPlaces {
			get;
		}
	}
}