/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Sequence series metadata.
    /// </summary>
    /// <remarks>
    /// The sequence series metadata are sent from the measurement device to the consuming device. It tells the
    /// information about the current series of sequences. The message is sent either when the measurement device
    /// is switched to measurement mode or when it is requested.
    /// </remarks>
    public interface ISequenceSeriesMetadata : IMessageBody
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        /// <value>
        /// The sequence that is going to be started.
        /// </value>
        string Sequence
        {
            get;
        }

        /// <summary>
        /// Gets the UUID.
        /// </summary>
        /// <value>
        /// A universally unique identifier according to <a href="">RFC 4122</a>. This identifies the sequence
        /// series. It is different for each series.
        /// </value>
        Guid Uuid
        {
            get;
        }

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>
        /// The measurements.
        /// </value>
        /// <seealso cref="IMeasurementMetadata"/>
#if MICRO_FRAMEWORK
        IDictionary Measurements
#else
        IDictionary<string, IMeasurementMetadata> Measurements
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the textual attributes.
        /// </summary>
        /// <value>
        /// The textual attributes that can be defined by the operator of the measurement device for manually
        /// initiated sequences or by the sequence series request for remotely initiated sequences.
        /// </value>
#if MICRO_FRAMEWORK
        IDictionary TextualAttributes
#else
        IDictionary<string, string> TextualAttributes
#endif
        {
            get;
        }

#if MICRO_FRAMEWORK
        IDictionary ResultColumnSettings
#else
        IDictionary<string, IList<ResultColumnSetting>> ResultColumnSettings
        {
            get;
        }
#endif
    }
}