/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif
using ProbeNet.Enums;

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Parameter description.
    /// </summary>
    public interface IParameterDescription
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier of the parameter. Must be unique within the measurement.
        /// </value>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// A human-readable name of the parameter in the language it is sent.
        /// </value>
        string Caption
        {
            get;
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit of the parameter. May be ommited if the parameter does not have a unit.
        /// </value>
        string Unit
        {
            get;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type of the parameter.
        /// </value>
        /// <seealso cref="ParameterType"/>
#if MICRO_FRAMEWORK
        string Type
#else
        ParameterType Type
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the minimum.
        /// </summary>
        /// <value>
        /// The lowest value that is allowed for the parameter.
        /// </value>
        double Minimum
        {
            get;
        }

        /// <summary>
        /// Gets the maximum.
        /// </summary>
        /// <value>
        /// The highest value that is allowed for the parameter.
        /// </value>
        double Maximum
        {
            get;
        }

        /// <summary>
        /// Gets the decimal places.
        /// </summary>
        /// <value>
        /// The number of places after the decimal point. Zero means that the value is an integer.
        /// </value>
        int DecimalPlaces
        {
            get;
        }

        /// <summary>
        /// Gets the enum values.
        /// </summary>
        /// <value>
        /// The possible values of the parameter. The list must contain at least one element. The items are specified
        /// in <see cref="IEnumerationValue"/> section.
        /// </value>
        /// <seealso cref="IEnumerationValue"/>
#if MICRO_FRAMEWORK
        IList EnumValues
#else
        IList<IEnumerationValue> EnumValues 
#endif
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ProbeNet.Messages.Interface.IParameterDescription"/> is editable.
        /// </summary>
        /// <value><c>true</c> if editable; otherwise, <c>false</c>.</value>
        bool Editable
        {
            get;
        }
    }
}