/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif
using ProbeNet.Enums;

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Curve description.
    /// </summary>
    public interface ICurveDescription
    {
        /// <summary>
        /// Gets the axes.
        /// </summary>
        /// <value>
        /// The list of axis descriptions.
        /// </value>
        /// <seealso cref="IAxisDescription"/>
#if MICRO_FRAMEWORK
        IList Axes
#else
        IList<IAxisDescription> Axes
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the coordinate system.
        /// </summary>
        /// <value>
        /// The coordinate system type.
        /// </value>
        /// <seealso cref="CoordinateSystemType"/>
#if MICRO_FRAMEWORK
        string CoordinateSystem
#else
        CoordinateSystemType CoordinateSystem
#endif
        {
            get;
        }
    }
}