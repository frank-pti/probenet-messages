/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Alignment description.
    /// </summary>
	public interface IAlignmentDescription
	{
        /// <summary>
        /// Gets or sets a value indicating whether the direction is relevant for the measurement.
        /// </summary>
        /// <value>
        /// <c>true</c> if the direction is relevant for the measurement; otherwise, <c>false</c>.
        /// </value>
		bool HasDirection {
			get;
			set;
		}
		
        /// <summary>
        /// Gets or sets a value indicating whether the side is relevant for the measurement.
        /// </summary>
        /// <value>
        /// <c>true</c> if the side is relevant for the measurement; otherwise, <c>false</c>.
        /// </value>
		bool HasSide {
			get;
			set;
		}
	}
}