/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Sequence description.
    /// </summary>
    public interface ISequenceDescription
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identification string for the sequence. It must be unique within the device description.
        /// </value>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// A human-readable name of the sequence in the language in which it is sent.
        /// </value>
        string Caption
        {
            get;
        }

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>
        /// The list must contain at least one element. The items are specified in the
        /// <see cref="ISequenceDescriptionMeasurementReference"/> section.
        /// </value>
        /// <seealso cref="ISequenceDescriptionMeasurementReference"/> 
#if MICRO_FRAMEWORK
        IList Measurements
#else
		IList<ISequenceDescriptionMeasurementReference> Measurements 
#endif
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether the sequence can be initiated remotely.
        /// </summary>
        /// <value>
        /// <c>true</c> if the sequence can be initiated remotely; otherwise, <c>false</c>.
        /// </value>
        bool CanBeInitiatedRemotely
        {
            get;
        }
    }
}