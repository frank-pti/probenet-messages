/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Enums;
#if !MICRO_FRAMEWORK
using System;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// The alignment of the measurement. Must be included if at least one of the fields is set <c>true</c> in the
    /// measurement Description.
    /// </summary>
    /// <seealso cref="IMeasurement"/>
    /// <seealso cref="IMeasurementDescription"/>
    public interface IAlignment
#if !MICRO_FRAMEWORK
 : IEquatable<IAlignment>
#endif
    {
#if MICRO_FRAMEWORK
        /// <summary>
        /// Gets or sets whether the value of the direction property is valid.
        /// </summary>
        /// <remarks>This is an alternative for the Nullable type missing in .NET MF</remarks>
        /// <value><code>true</code> if the direction value is valid, <code>false</code> otherwise.</value>
        bool DirectionValid
        {
            get;
            set;
        }
#endif

        /// <summary>
        /// Gets or sets the direction.
        /// </summary>
        /// <value>
        /// The direction of the sample in degrees. A value of 0 means machine direction, a value of 90 degrees
        /// means cross direction. Must be included when the <see cref="IAlignmentDescription.HasDirection"/>
        /// property is set to <c>true</c>. The minimum value is 0, the maximum value is 359.
        /// </value>
#if MICRO_FRAMEWORK
        int Direction
#else
        int? Direction
#endif
        {
            get;
            set;
        }

#if MICRO_FRAMEWORK
        /// <summary>
        /// Gets or sets whether the value of the sample side property is valid.
        /// </summary>
        /// <remarks>This is an alternative for the Nullable type missing in .NET MF</remarks>
        /// <value><code>true</code> if the sample side value is valid, <code>false</code> otherwise.</value>
        bool SideValid
        {
            get;
            set;
        }
#endif

        /// <summary>
        /// Gets or sets the side.
        /// </summary>
        /// <value>
        /// The side of the sample where the measurement was performed. Must be included when the
        /// <see cref="IAlignmentDescription.HasSide"/> property is set to <c>true</c>.
        /// </value>
#if MICRO_FRAMEWORK
        SampleSide Side
#else
        SampleSide? Side
#endif
        {
            get;
            set;
        }
    }
}