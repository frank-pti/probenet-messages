/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// The sequence message.
    /// </summary>
    /// <remarks>
    ///  The sequence message is sent after a sequence was executed. It contains the results of its measurements.
    /// </remarks>
    public interface ISequence : IMessageBody
    {
        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>
        /// Dictionary that contains a measurement object for each measurement id.
        /// </value>
        /// <seealso cref="IMeasurement"/>
#if MICRO_FRAMEWORK
        IDictionary Measurements
#else
		IDictionary<string, IMeasurement> Measurements
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the moment.
        /// </summary>
        /// <value>
        /// The moment when the sequence executing was started, or if that moment can't be exactly determined, the
        /// nearest moment in time that can be determined. The format is according to time stamp definition in the
        /// ProbeNet protocol specification. If the measurement device has a system clock, it must always includ
        /// this field. If a measurement device has no system clock, it must take the system time from the header of
        /// the first message that it receives form the consuming device and count the clock from that moment on. This
        /// means the field is mandatory as soon as the consuming device sent a valid message to the measurement
        /// device.
        /// </value>
        DateTime Moment
        {
            get;
        }

        /// <summary>
        /// Gets the UUID.
        /// </summary>
        /// <value>
        /// A universally unique identifier according to <a href="">RFC 4122</a>. This identifiers the sequence. It is
        /// different for each sequence.
        /// </value>
        Guid Uuid
        {
            get;
        }

        /// <summary>
        /// Gets the textual attributes.
        /// </summary>
        /// <value>
        /// The textual attributes.
        /// </value>
#if MICRO_FRAMEWORK
        IDictionary TextualAttributes
#else
		IDictionary<string, string> TextualAttributes
#endif
        {
            get;
        }
    }
}