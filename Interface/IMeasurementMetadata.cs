/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Measurement metadata.
    /// </summary>
    public interface IMeasurementMetadata
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="IMeasurementMetadata"/> is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        bool Enabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters which types are either boolean, numeric or string (for enumeration values) according to
        /// parameter description.
        /// </value>
        /// <seealso cref="IParameterDescription"/>
#if MICRO_FRAMEWORK
        IDictionary Parameters
#else
        IDictionary<string, object> Parameters
#endif
        {
            get;
            set;
        }
    }
}