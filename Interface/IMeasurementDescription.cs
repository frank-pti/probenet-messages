/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Measurement description.
    /// </summary>
    public interface IMeasurementDescription
    {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identification string for the measurement. It must be unique within the device description.
        /// </value>
        string Id
        {
            get;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>
        /// A human-readable name of the measurement in the language in which it is sent.
        /// </value>
        string Caption
        {
            get;
        }

        /// <summary>
        /// Gets the short caption.
        /// </summary>
        /// <value>
        /// A human-readable short name of the measurement in the language in which it is sent.
        /// Maximum length: 10 characters.
        /// </value>
        string ShortCaption
        {
            get;
        }

        /// <summary>
        /// Gets the technical standards.
        /// </summary>
        /// <value>
        /// The technical standards.
        /// </value>
#if MICRO_FRAMEWORK
        IList TechnicalStandards
#else
        IList<string> TechnicalStandards
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the alignment description.
        /// </summary>
        /// <value>
        /// The alignment description is specified in the <see cref="IAlignmentDescription"/> section.
        /// </value>
        /// <seealso cref="IAlignmentDescription"/>
        IAlignmentDescription AlignmentDescription
        {
            get;
        }

        /// <summary>
        /// Gets the parameter descriptions.
        /// </summary>
        /// <value>
        /// The parameter description is specified in the <see cref="IParameterDescription"/> section.
        /// </value>
        /// <seealso cref="IParameterDescription"/>
#if MICRO_FRAMEWORK
        IList ParameterDescriptions
#else
		IList<IParameterDescription> ParameterDescriptions
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the result descriptions.
        /// </summary>
        /// <value>
        /// The list must contain at least one element. The result description is specified in the
        /// <see cref="IResultDescription"/> section.
        /// </value>
        /// <seealso cref="IResultDescription"/>
#if MICRO_FRAMEWORK
        IList ResultDescriptions
#else
		IList<IResultDescription> ResultDescriptions
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the curve description.
        /// </summary>
        /// <value>
        /// The curve description is specified in the <see cref="ICurveDescription"/> section.
        /// </value>
        /// <seealso cref="ICurveDescription"/>
        ICurveDescription CurveDescription
        {
            get;
        }

        /// <summary>
        /// Gets the image descriptions.
        /// </summary>
        /// <value>
        /// The image description is specified in the <see cref="IImageDescription"/> section.
        /// </value>
        /// <seealso cref="IImageDescription"/>
#if MICRO_FRAMEWORK
        IList ImageDescriptions
#else
		IList<IImageDescription> ImageDescriptions
#endif
        {
            get;
        }
    }
}