/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
#endif

namespace ProbeNet.Messages.Interface
{
    /// <summary>
    /// Sequence description measurement reference.
    /// </summary>
    public interface ISequenceDescriptionMeasurementReference
    {
        /// <summary>
        /// Gets the designation.
        /// </summary>
        /// <value>
        /// An id of this object. Must be unique within the array.
        /// </value>
        string Designation
        {
            get;
        }

        /// <summary>
        /// Gets the measurement.
        /// </summary>
        /// <value>
        /// The referenced measurement description.
        /// </value>
        /// <seealso cref="IMeasurementDescription"/>
        IMeasurementDescription Measurement
        {
            get;
        }

        /// <summary>
        /// Gets the depends.
        /// </summary>
        /// <value>
        /// The list of the designations that this measurement depends on.
        /// </value>
#if MICRO_FRAMEWORK
        IList Depends
#else
		IList<string> Depends
#endif
        {
            get;
        }
    }
}