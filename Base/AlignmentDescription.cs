/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("alignmentDescription")]
#endif
    public abstract class AlignmentDescription : IAlignmentDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.AlignmentDescription"/> class.
        /// </summary>
        protected AlignmentDescription() :
            this(false, false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.AlignmentDescription"/> class.
        /// </summary>
        /// <param name="hasDirection">If set to <c>true</c> has direction.</param>
        /// <param name="hasSide">If set to <c>true</c> has side.</param>
        protected AlignmentDescription(bool hasDirection, bool hasSide)
        {
            HasDirection = hasDirection;
            HasSide = hasSide;
        }

        #region IAlignmentDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("hasDirection")]
        [YAXSerializableField]
        [YAXSerializeAs("hasDirection")]
        [YAXAttributeForClass]
#endif
        public bool HasDirection
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("hasSide")]
        [YAXSerializableField]
        [YAXSerializeAs("hasSide")]
        [YAXAttributeForClass]
#endif
        public bool HasSide
        {
            get;
            set;
        }
        #endregion
    }
}