#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public abstract class SequenceDescriptionMeasurementReference<TMeasurementDescription>:
        Base.SequenceDescriptionMeasurementReference
            where TMeasurementDescription: Base.MeasurementDescription
    {
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.Base.Generic.SequenceDescriptionMeasurementReference`1"/> class.
        /// </summary>
        /// <param name="designation">Designation.</param>
        /// <param name="measurementId">Measurement identifier.</param>
        public SequenceDescriptionMeasurementReference(string designation, string measurementId):
            base(designation, measurementId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.Base.Generic.SequenceDescriptionMeasurementReference`1"/> class.
        /// </summary>
        /// <param name="designation">Designation.</param>
        /// <param name="measurement">Measurement.</param>
        public SequenceDescriptionMeasurementReference(string designation, TMeasurementDescription measurement):
            base(designation, measurement.Id)
        {
            Measurement = measurement;
        }

#region implemented abstract members of ProbeNet.Messages.Base.SequenceDescriptionMeasurementReference
        /// <inheritdoc/>
        protected override MeasurementDescription BaseMeasurement {
            get {
                return Measurement;
            }
        }
        #endregion

        /// <inheritdoc/>
        public new TMeasurementDescription Measurement {
            get;
            set;
        }
    }
}
#endif
