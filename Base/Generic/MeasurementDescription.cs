#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    public abstract class MeasurementDescription<
        TAlignmentDescription, TParameterDescription, TResultDescription, TCurveDescription, TImageDescription>:
            Base.MeasurementDescription
        where TAlignmentDescription: Base.AlignmentDescription
        where TParameterDescription: Base.ParameterDescription
        where TResultDescription: Base.ResultDescription
        where TCurveDescription: Base.CurveDescription
        where TImageDescription: Base.ImageDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.MeasurementDescription`5"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        protected MeasurementDescription(string id):
            base(id)
        {
        }

#region implemented abstract members of ProbeNet.Messages.Base.MeasurementDescription
        /// <inheritdoc/>
        protected override IList<ParameterDescription> BaseParameterDescriptions {
            get {
                return ListCastAdapter<TParameterDescription, Base.ParameterDescription>.Build(ParameterDescriptions);
            }
        }

        /// <inheritdoc/>
        protected override IList<ResultDescription> BaseResultDescriptions {
            get {
                return ListCastAdapter<TResultDescription, Base.ResultDescription>.Build (ResultDescriptions);
            }
        }

        /// <inheritdoc/>
        protected override Base.AlignmentDescription BaseAlignmentDescription {
            get {
                return AlignmentDescription;
            }
        }

        /// <inheritdoc/>
        protected override Base.CurveDescription BaseCurveDescription {
            get {
                return CurveDescription;
            }
        }

        /// <inheritdoc/>
        protected override IList<ImageDescription> BaseImageDescriptions {
            get {
                return ListCastAdapter<TImageDescription, Base.ImageDescription>.Build (ImageDescriptions);
            }
        }
        #endregion

        /// <inheritdoc/>
        [JsonProperty("parameterDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("parameterDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "parameter")]
        public new IList<TParameterDescription> ParameterDescriptions {
            get;
            set;
        }

        /// <inheritdoc/>
        [JsonProperty("resultDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("resultDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "result")]
        public new IList<TResultDescription> ResultDescriptions {
            get;
            set;
        }

        /// <inheritdoc/>
        [JsonProperty("alignmentDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("alignmentDescription")]
        public virtual new TAlignmentDescription AlignmentDescription
        {
            get;
            set;
        }

        /// <inheritdoc/>
        [JsonProperty("curveDescription")]
        [YAXSerializableField]
        [YAXSerializeAs("curveDescription")]
        public virtual new TCurveDescription CurveDescription {
            get;
            set;
        }

        /// <inheritdoc/>
        [JsonProperty("imageDescriptions")]
        [YAXSerializableField]
        [YAXSerializeAs("imageDescriptions")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "image")]
        public new IList<TImageDescription> ImageDescriptions {
            get;
            set;
        }
    }
}
#endif
