#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public class Sequence<TMeasurement>:
        Base.Sequence
        where TMeasurement: Base.Measurement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.Sequence`1"/> class.
        /// </summary>
        public Sequence():
            this(Guid.NewGuid(), DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.Sequence`1"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        public Sequence(Guid uuid):
            this(uuid, DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.Sequence`1"/> class.
        /// </summary>
        /// <param name="moment">Moment.</param>
        public Sequence(DateTime moment):
            this(Guid.NewGuid(), moment)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.Sequence`1"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        /// <param name="moment">Moment.</param>
        public Sequence(Guid uuid, DateTime moment)
        {
            Measurements = new Dictionary<string, TMeasurement>();
        }

#region implemented abstract members of ProbeNet.Messages.Base.Sequence
        /// <inheritdoc/>
        protected override IDictionary<string, Base.Measurement> BaseMeasurements {
            get {
                return DictionaryCastAdapter<string, TMeasurement, Base.Measurement>.Build(Measurements);
            }
        }
        #endregion

        /// <inheritdoc/>
        [JsonProperty("measurements")]
        [YAXSerializeAs("measurements")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "measurement",
            KeyName = "id",
            SerializeKeyAs = YAXNodeTypes.Attribute,
            ValueName = "measurement_data",
            SerializeValueAs = YAXNodeTypes.Element)]
        public new IDictionary<string, TMeasurement> Measurements {
            get;
            set;
        }
    }
}
#endif
