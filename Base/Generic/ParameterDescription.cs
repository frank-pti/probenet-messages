#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Enums;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public abstract class ParameterDescription<TEnumerationValue>:
        Base.ParameterDescription
            where TEnumerationValue: Base.EnumerationValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.ParameterDescription`1"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="type">Type.</param>
        protected ParameterDescription (string id, ParameterType type):
            base(id, type)
        {
        }

        /// <inheritdoc/>
        protected override IList<EnumerationValue> BaseEnumValues {
            get {
                return ListCastAdapter<TEnumerationValue, Base.EnumerationValue>.Build(EnumValues);
            }
        }

        /// <inheritdoc/>
        [JsonProperty("enumValues")]
        [YAXSerializableField]
        [YAXSerializeAs("enumValues")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "enumValue")]
        public virtual new IList<TEnumerationValue> EnumValues {
            get;
            set;
        }
    }
}
#endif
