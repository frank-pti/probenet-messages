#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Newtonsoft.Json;
using YAXLib;
using System.Collections.Generic;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public class SequenceSeriesMetadata<TMeasurementMetadata>:
        Base.SequenceSeriesMetadata
        where TMeasurementMetadata: Base.MeasurementMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.SequenceSeriesMetadata`1"/> class.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        /// <param name="uuid">UUID.</param>
        public SequenceSeriesMetadata(string sequence, Guid uuid):
            base(sequence, uuid)
        {
        }

        /// <inheritdoc/>
        [JsonProperty("measurements")]
        [YAXSerializeAs("measurements")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "measurement",
            KeyName = "designation",
            SerializeKeyAs = YAXNodeTypes.Attribute,
            ValueName = "measurementMetadata",
            SerializeValueAs = YAXNodeTypes.Element)]
        public new IDictionary<string, TMeasurementMetadata> Measurements {
            get;
            set;
        }

#region implemented abstract members of ProbeNet.Messages.Base.SequenceSeriesMetadata
        /// <inheritdoc/>
        protected override IDictionary<string, MeasurementMetadata> BaseMeasurements {
            get {
                return DictionaryCastAdapter<string, TMeasurementMetadata, MeasurementMetadata>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif
