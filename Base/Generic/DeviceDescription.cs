#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public abstract class DeviceDescription<TMeasurementDescription, TSequenceDescription>: DeviceDescription
        where TMeasurementDescription: Base.MeasurementDescription
        where TSequenceDescription: Base.SequenceDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.DeviceDescription`2"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
        protected DeviceDescription (string id, Guid uuid):
            base(id, uuid)
        {
            Measurements = new List<TMeasurementDescription>();
            Sequences = new List<TSequenceDescription>();
        }

#region implemented abstract members of ProbeNet.Messages.Base.DeviceDescription
        /// <inheritdoc/>
        protected override IList<Base.MeasurementDescription> BaseMeasurements {
            get {
                return ListCastAdapter<TMeasurementDescription, Base.MeasurementDescription>.Build(Measurements);
            }
        }

        /// <inheritdoc/>
        protected override IList<Base.SequenceDescription> BaseSequences {
            get {
                return ListCastAdapter<TSequenceDescription, Base.SequenceDescription>.Build(Sequences);
            }
        }
        #endregion

        /// <inheritdoc/>
        [JsonProperty("measurements", Required = Required.Default)]
        public virtual new IList<TMeasurementDescription> Measurements {
            get;
            set;
        }

        /// <inheritdoc/>
        [JsonProperty("sequences", Required = Required.Default)]
        public virtual new IList<TSequenceDescription> Sequences {
            get;
            set;
        }
    }
}
#endif
