#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using ProbeNet.Enums;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public abstract class CurveDescription<TAxisDescription>: Base.CurveDescription
        where TAxisDescription: Base.AxisDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.CurveDescription`1"/> class.
        /// </summary>
        protected CurveDescription():
            this(CoordinateSystemType.Cartesian, new List<TAxisDescription>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.CurveDescription`1"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        protected CurveDescription (CoordinateSystemType coordinateSystem):
            this(coordinateSystem, new List<TAxisDescription>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.CurveDescription`1"/> class.
        /// </summary>
        /// <param name="axes">Axes.</param>
        protected CurveDescription(IList<TAxisDescription> axes):
            this(CoordinateSystemType.Cartesian, axes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.CurveDescription`1"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        /// <param name="axes">Axes.</param>
        protected CurveDescription(CoordinateSystemType coordinateSystem, IList<TAxisDescription> axes):
            base(coordinateSystem)
        {
            Axes = axes;
        }

        /// <inheritdoc/>
        [JsonProperty("axes")]
        [YAXSerializableField]
        [YAXSerializeAs("axes")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "axisDescription")]
        public virtual new IList<TAxisDescription> Axes {
            get;
            set;
        }

#region implemented abstract members of ProbeNet.Messages.Base.CurveDescription
        /// <inheritdoc/>
        protected override IList<Base.AxisDescription> BaseAxes {
            get {
                return ListCastAdapter<TAxisDescription, Base.AxisDescription>.Build(Axes);
            }
        }
        #endregion

    }
}
#endif
