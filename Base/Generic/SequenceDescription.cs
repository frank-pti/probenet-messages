#if !MICRO_FRAMEWORK
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;

namespace ProbeNet.Messages.Base.Generic
{
    /// <inheritdoc/>
    public abstract class SequenceDescription<TSequenceDescriptionMeasurementReference>:
        Base.SequenceDescription
            where TSequenceDescriptionMeasurementReference: Base.SequenceDescriptionMeasurementReference
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Generic.SequenceDescription`1"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public SequenceDescription(string id):
            base(id)
        {
        }

        /// <inheritdoc/>
        [JsonProperty("measurements")]
        [YAXSerializeAs("measurements")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "measurement")]
        public virtual new IList<TSequenceDescriptionMeasurementReference> Measurements {
            get;
            set;
        }

#region implemented abstract members of ProbeNet.Messages.Base.SequenceDescription
        /// <inheritdoc/>
        protected override IList<SequenceDescriptionMeasurementReference> BaseMeasurements {
            get {
                return ListCastAdapter<TSequenceDescriptionMeasurementReference, Base.SequenceDescriptionMeasurementReference>.Build(Measurements);
            }
        }
        #endregion
    }
}
#endif
