/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("reference")]
#endif
    public abstract class SequenceDescriptionMeasurementReference : ISequenceDescriptionMeasurementReference
    {
        private string measurementId;

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="ProbeNet.Messages.Base.SequenceDescriptionMeasurementReference"/> class.
        /// </summary>
        /// <param name="designation">Designation.</param>
        /// <param name="measurementId">Measurement identifier.</param>
        protected SequenceDescriptionMeasurementReference(string designation, string measurementId)
        {
            Designation = designation;
            MeasurementId = measurementId;
        }

        #region ISequenceDescriptionMeasurementReference implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("designation")]
        [YAXSerializeAs("designation")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string Designation
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("measurementId")]
        [YAXSerializeAs("measurement")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string MeasurementId
        {
            get
            {
                if (Measurement == null || Measurement.Id == null) {
                    return measurementId;
                }
                return Measurement.Id;
            }
            set
            {
                if (Measurement != null) {
                    BaseMeasurement.Id = value;
                }
                measurementId = value;
            }
        }

        /// <summary>
        /// Gets the base measurement.
        /// </summary>
        /// <value>The base measurement.</value>
        protected abstract MeasurementDescription BaseMeasurement
        {
            get;
        }

        /// <inheritdoc/>
        public IMeasurementDescription Measurement
        {
            get
            {
                return BaseMeasurement;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList Depends
#else
        [JsonProperty("depends")]
        [YAXSerializableField]
        [YAXCollection(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "depends")]
        public IList<string> Depends
#endif
        {
            get;
            set;
        }
        #endregion
    }
}