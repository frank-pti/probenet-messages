/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Messages;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("sequence")]
#endif
    public abstract class Sequence : MessageBody, ISequence
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Sequence"/> class.
        /// </summary>
        protected Sequence() :
            this(Guid.NewGuid())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        protected Sequence(Guid uuid) :
            this(uuid, DateTime.Now)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Sequence"/> class.
        /// </summary>
        /// <param name="moment">Moment.</param>
        protected Sequence(DateTime moment) :
            this(Guid.NewGuid(), moment)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Sequence"/> class.
        /// </summary>
        /// <param name="uuid">UUID.</param>
        /// <param name="moment">Moment.</param>
        protected Sequence(Guid uuid, DateTime moment) :
            base(MessageType.Sequence)
        {
            Uuid = uuid;
            Moment = moment;
        }

        #region ISequence implementation
#if !MICRO_FRAMEWORK
        IDictionary<string, IMeasurement> ISequence.Measurements
        {
            get {
                return DictionaryCastAdapter<string, Measurement, IMeasurement>.Build(Measurements);
            }
        }
#endif

        /// <summary>
        /// Gets the base measurements.
        /// </summary>
        /// <value>The base measurements.</value>
#if MICRO_FRAMEWORK
        protected abstract IDictionary BaseMeasurements
#else
        protected abstract IDictionary<string, Measurement> BaseMeasurements
#endif
        {
            get;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary Measurements
#else
        public IDictionary<string, Measurement> Measurements
#endif
        {
            get
            {
                return BaseMeasurements;
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("moment")]
        [JsonConverter(typeof(ProbeNetDateTimeConverter))]
        [YAXSerializeAs("moment")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public DateTime Moment
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("uuid")]
        [YAXSerializeAs("uuid")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public Guid Uuid
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary TextualAttributes
#else
        [JsonProperty("textualAttributes")]
        [YAXSerializeAs("textualAttributes")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "attribute",
            KeyName = "name",
            SerializeKeyAs = YAXNodeTypes.Element,
            ValueName = "value",
            SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, string> TextualAttributes
#endif
        {
            get;
            set;
        }
        #endregion
    }
}