/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if MICRO_FRAMEWORK
using System.Collections;
using ProbeNet.Enums.Converting;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif
using ProbeNet.Enums;
using ProbeNet.Messages.Interface;

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("sequenceDescription")]
#endif
    public abstract class SequenceDescription : MessageBody, ISequenceDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.SequenceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        protected SequenceDescription(string id) :
            base(MessageType.SequenceDescription)
        {
            Id = id;
            CanBeInitiatedRemotely = false;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
        protected abstract string CaptionString
        {
            get;
        }

        #region ISequenceDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializeAs("id")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public virtual string Id
        {
            get;
            set;
        }

#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        string ISequenceDescription.Caption
        {
            get
            {
                return CaptionString;
            }
        }

#if !MICRO_FRAMEWORK
        IList<ISequenceDescriptionMeasurementReference> ISequenceDescription.Measurements
        {
            get
            {
                return ListCastAdapter<SequenceDescriptionMeasurementReference, ISequenceDescriptionMeasurementReference>.Build(Measurements);
            }
        }
#endif

        /// <summary>
        /// Gets the base measurements.
        /// </summary>
        /// <value>The base measurements.</value>
#if MICRO_FRAMEWORK
        protected abstract IList BaseMeasurements
#else
        protected abstract IList<SequenceDescriptionMeasurementReference> BaseMeasurements
#endif
        {
            get;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList Measurements
#else
        [YAXDontSerialize]
        public IList<SequenceDescriptionMeasurementReference> Measurements
#endif
        {
            get
            {
                return BaseMeasurements;
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("canBeInitiatedRemotely")]
        [YAXSerializeAs("canBeInitiatedRemotely")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public virtual bool CanBeInitiatedRemotely
        {
            get;
            set;
        }
        #endregion
    }
}