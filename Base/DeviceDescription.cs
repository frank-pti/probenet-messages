/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("deviceDescription")]
#endif
    public abstract class DeviceDescription : MessageBody, IDeviceDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.DeviceDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="uuid">UUID.</param>
        protected DeviceDescription(string id, Guid uuid) :
            base(MessageType.DeviceDescription)
        {
            Id = id;
            Uuid = uuid;
#if MICRO_FRAMEWORK
            InstalledLanguages = new ArrayList();
#else
            InstalledLanguages = new List<string>();
#endif
        }

        /// <summary>
        /// Gets the caption
        /// </summary>
        /// <value>The caption.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        protected abstract string CaptionString
        {
            get;
        }

        #region IDeviceDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
#endif
        public virtual string Id
        {
            get;
            set;
        }

        /// <inheritdoc/>
        string IDeviceDescription.Caption
        {
            get
            {
                return CaptionString;
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("probeNetVersion")]
#endif
        public virtual int ProbeNetVersion
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("modelNumber")]
#endif
        public virtual string ModelNumber
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("serialNumber")]
#endif
        public virtual string SerialNumber
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("firmwareVersion")]
#endif
        public virtual string FirmwareVersion
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("manufacturer")]
#endif
        public virtual string Manufacturer
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public virtual IList Extensions
#else
        [JsonProperty("extensions")]
        public virtual IList<string> Extensions
#endif
        {
            get;
            set;
        }

#if !MICRO_FRAMEWORK
        IList<IMeasurementDescription> IDeviceDescription.Measurements
        {
            get
            {
                return ListCastAdapter<MeasurementDescription, IMeasurementDescription>.Build(Measurements);
            }
        }

        IList<ISequenceDescription> IDeviceDescription.Sequences
        {
            get
            {
                return ListCastAdapter<SequenceDescription, ISequenceDescription>.Build(Sequences);
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public virtual IList InstalledLanguages
#else
        [JsonProperty("installedLanguages")]
        public virtual IList<string> InstalledLanguages
#endif
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("icon")]
#endif
        public virtual byte[] Icon
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("uuid")]
#endif
        public virtual Guid Uuid
        {
            get;
            set;
        }
        #endregion

        /// <summary>
        /// Gets the base measurements.
        /// </summary>
        /// <value>The base measurements.</value>
        /// <seealso cref="IMeasurementDescription"/>
#if MICRO_FRAMEWORK
        protected abstract int BaseMeasurementCount
        {
            get;
        }
#else
        protected abstract IList<MeasurementDescription> BaseMeasurements
        {
            get;
        }
#endif

        /// <summary>
        /// Gets the measurements.
        /// </summary>
        /// <value>The measurements.</value>
        /// <seealso cref="IMeasurementDescription"/>
#if MICRO_FRAMEWORK       
        public int MeasurementCount
        {
            get
            {
                return BaseMeasurementCount;
            }
        }
#else
        public IList<MeasurementDescription> Measurements
        {
            get
            {
                return BaseMeasurements;
            }
        }
#endif

        /// <summary>
        /// Gets the base sequences.
        /// </summary>
        /// <value>The base sequences.</value>
        /// <seealso cref="ISequenceDescription"/>
#if MICRO_FRAMEWORK
        protected abstract int BaseSequenceCount
#else
        protected abstract IList<SequenceDescription> BaseSequences
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the sequence descriptions.
        /// </summary>
        /// <value>The sequence descriptions.</value>
        /// <seealso cref="ISequenceDescription"/>
#if MICRO_FRAMEWORK
        public int SequenceCount
        {
            get
            {
                return BaseSequenceCount;
            }
        }
#else
        public IList<SequenceDescription> Sequences
        {
            get
            {
                return BaseSequences;
            }
        }
#endif

        /// <summary>
        /// Gets a value indicating whether this device has an icon.
        /// </summary>
        /// <value><c>true</c> if this device has an icon; otherwise, <c>false</c>.</value>
        public bool HasIcon
        {
            get
            {
                return Icon != null && Icon.Length > 0;
            }
        }
    }
}