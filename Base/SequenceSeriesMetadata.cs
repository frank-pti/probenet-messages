/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("metadata")]
#endif
    public abstract class SequenceSeriesMetadata : MessageBody, ISequenceSeriesMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.SequenceSeriesMetadata"/> class.
        /// </summary>
        /// <param name="sequence">Sequence.</param>
        /// <param name="uuid">UUID.</param>
        protected SequenceSeriesMetadata(string sequence, Guid uuid) :
            base(MessageType.SequenceSeriesMetadata)
        {
            Sequence = sequence;
            Uuid = uuid;
        }

        #region ISequenceSeriesMetadata implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("sequence")]
        [YAXSerializeAs("sequence")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public string Sequence
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("uuid")]
        [YAXSerializeAs("uuid")]
        [YAXSerializableField]
        [YAXAttributeForClass]
#endif
        public Guid Uuid
        {
            get;
            set;
        }

#if !MICRO_FRAMEWORK
        IDictionary<string, IMeasurementMetadata> ISequenceSeriesMetadata.Measurements
        {
            get
            {
                return DictionaryCastAdapter<string, MeasurementMetadata, IMeasurementMetadata>.Build(Measurements);
            }
        }
#endif

        /// <summary>
        /// Gets the base measurements.
        /// </summary>
        /// <value>The base measurements.</value>
#if MICRO_FRAMEWORK
        protected abstract IDictionary BaseMeasurements
#else
        protected abstract IDictionary<string, MeasurementMetadata> BaseMeasurements
#endif
        {
            get;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary Measurements
#else
        public IDictionary<string, MeasurementMetadata> Measurements
#endif
        {
            get
            {
                return BaseMeasurements;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IDictionary TextualAttributes
#else
        [JsonProperty("textualAttributes")]
        [YAXSerializeAs("textualAttributes")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "attribute",
            KeyName = "name",
            SerializeKeyAs = YAXNodeTypes.Element,
            ValueName = "value",
            SerializeValueAs = YAXNodeTypes.Element)]
        public IDictionary<string, string> TextualAttributes
#endif
        {
            get;
            set;
        }

#if MICRO_FRAMEWORK
        public IDictionary ResultColumnSettings
#else
        [JsonProperty("resultColumnSettings")]
        [YAXSerializeAs("resultColumnSettings")]
        [YAXSerializableField]
        [YAXDictionary(
            EachPairName = "setting",
            KeyName = "id",
            SerializeKeyAs = YAXNodeTypes.Element,
            ValueName = "value",
            SerializeValueAs = YAXNodeTypes.Content)]
        public IDictionary<string, IList<ResultColumnSetting>> ResultColumnSettings
#endif
        {
            get;
            set;
        }
        #endregion
    }
}