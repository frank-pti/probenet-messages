/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ProbeNet.Enums;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using System.Collections;
#else
using Newtonsoft.Json;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
    public abstract class SequenceDescriptionRequest : MessageBody, ISequenceDescriptionRequest
    {
        /// <summary>
        /// Initializes a new instace of type <see cref="ProbeNet.Messages.Base.SequenceDescriptionsRequest"/>
        /// </summary>
        /// <param name="index">INdex of the sequence description that is requested.</param>
        protected SequenceDescriptionRequest(int index) :
            base(MessageType.SequenceDescriptionRequest)
        {
            Index = index;
        }

#if MICRO_FRAMEWORK
        /// <summary>
        /// Initializes a new instance and set the values according to the deserialized hashtable.
        /// </summary>
        /// <param name="deserialized">The deserialized values.</param>
        protected SequenceDescriptionRequest(Hashtable deserialized) :
            this(ExtractIndex(deserialized))
        {
        }

        private static int ExtractIndex(Hashtable deserilized)
        {
            long longValue = (long)deserilized["index"];
            return (int)longValue;
        }
#endif

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("index")]
#endif
        public abstract int Index { get; set; }
    }
}
