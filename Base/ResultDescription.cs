/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Enums;
using ProbeNet.Messages.Interface;
#if MICRO_FRAMEWORK
using ProbeNet.Enums.Converting;
using System.Collections;
#else
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
using System;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("resultDescription")]
#endif
    public abstract class ResultDescription : IResultDescription
    {
        private SourceType source;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.ResultDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="source">Source.</param>
        protected ResultDescription(string id, SourceType source)
        {
            Id = id;
            this.source = source;
            Minimum = double.MinValue;
            Maximum = double.MaxValue;
            DecimalPlaces = 3;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        protected abstract string CaptionString
        {
            get;
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>The unit.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        protected abstract string UnitString
        {
            get;
        }

        #region IResultDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get;
            set;
        }

        string IResultDescription.Caption
        {
            get
            {
                return CaptionString;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Source
        {
            get
            {
                return (string)SourceTypeConverter.Serialize(source);
            }
            set
            {
                source = SourceTypeConverter.Deserialize(value);
            }
        }
#else
        [JsonProperty("source")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<SourceType>))]
        [YAXSerializableField]
        [YAXSerializeAs("source")]
        [YAXAttributeForClass]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<SourceType>))]
        public SourceType Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }
#endif

        string IResultDescription.Unit
        {
            get
            {
                return UnitString;
            }
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("minimum")]
        [YAXSerializableField]
        [YAXSerializeAs("minimum")]
        [YAXAttributeForClass]
#endif
        public double Minimum
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("maximum")]
        [YAXSerializableField]
        [YAXSerializeAs("maximum")]
        [YAXAttributeForClass]
#endif
        public double Maximum
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("decimalPlaces")]
        [YAXSerializableField]
        [YAXSerializeAs("decimalPlaces")]
        [YAXAttributeForClass]
#endif
        public int DecimalPlaces
        {
            get;
            set;
        }
        #endregion

        /// <summary>
        /// Gets a value indicating whether this result description has a unit.
        /// </summary>
        /// <value><c>true</c> if this result description has a unit; otherwise, <c>false</c>.</value>
        public bool HasUnit
        {
            get
            {
#if MICRO_FRAMEWORK
                return UnitString != null && UnitString.Trim().Length > 0;
#else
                return !String.IsNullOrWhiteSpace(UnitString);
#endif
            }
        }
    }
}