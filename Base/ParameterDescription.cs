/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
using ProbeNet.Enums.Converting;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
using System;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("parameterDescription")]
#endif
    public abstract class ParameterDescription : IParameterDescription
    {
        private ParameterType type;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.ParameterDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="type">Type.</param>
        protected ParameterDescription(string id, ParameterType type)
        {
            Id = id;
            Editable = false;
            this.type = type;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        protected abstract string CaptionString
        {
            get;
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>The unit.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("unit")]
        [YAXSerializableField]
        [YAXSerializeAs("unit")]
        [YAXAttributeForClass]
#endif
        protected abstract string UnitString
        {
            get;
        }

        #region IParameterDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get;
            set;
        }

        string IParameterDescription.Caption
        {
            get
            {
                return CaptionString;
            }
        }

        string IParameterDescription.Unit
        {
            get
            {
                return UnitString;
            }
        }

        /// <inheritdoc/>
        public bool HasUnit
        {
            get
            {
#if MICRO_FRAMEWORK
                return UnitString != null && UnitString.Trim().Length > 0;
#else
                return !String.IsNullOrWhiteSpace(UnitString);
#endif
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string Type
        {
            get
            {
                return (string)ParameterTypeConverter.Serialize(type);
            }
            set
            {
                type = ParameterTypeConverter.Deserialize(value);
            }
        }
#else
        [JsonProperty("type")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<ParameterType>))]
        [YAXSerializableField]
        [YAXSerializeAs("type")]
        [YAXAttributeForClass]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<ParameterType>))]
        public ParameterType Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
#endif

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("minimum")]
        [YAXSerializableField]
        [YAXSerializeAs("minimum")]
        [YAXAttributeForClass]
#endif
        public double Minimum
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("maximum")]
        [YAXSerializableField]
        [YAXSerializeAs("maximum")]
        [YAXAttributeForClass]
#endif
        public double Maximum
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("decimalPlaces")]
        [YAXSerializableField]
        [YAXSerializeAs("decimalPlaces")]
        [YAXAttributeForClass]
#endif
        public int DecimalPlaces
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("editable")]
        [YAXSerializableField]
        [YAXSerializeAs("editable")]
        [YAXAttributeForClass]
#endif
        public bool Editable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the base enum values.
        /// </summary>
        /// <value>The base enum values.</value>
        /// <seealso cref="IEnumerationValue"/>
#if MICRO_FRAMEWORK
        protected abstract IList BaseEnumValues
#else
        protected abstract IList<EnumerationValue> BaseEnumValues
#endif
        {
            get;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList EnumValues
#else
        [YAXDontSerialize]
        public IList<EnumerationValue> EnumValues
#endif
        {
            get
            {
                return BaseEnumValues;
            }
        }

#if !MICRO_FRAMEWORK
        IList<IEnumerationValue> IParameterDescription.EnumValues
        {
            get
            {
                return ListCastAdapter<EnumerationValue, IEnumerationValue>.Build(EnumValues);
            }
        }
#endif
        #endregion
    }
}