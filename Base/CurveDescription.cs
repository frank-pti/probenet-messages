/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
using ProbeNet.Enums.Converting;
#else
using Frank.Helpers.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("curveDescription")]
#endif
    public abstract class CurveDescription : ICurveDescription
    {
        private CoordinateSystemType coordinateSystem;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.CurveDescription"/> class.
        /// </summary>
        protected CurveDescription() :
            this(CoordinateSystemType.Cartesian)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.CurveDescription"/> class.
        /// </summary>
        /// <param name="coordinateSystem">Coordinate system.</param>
        protected CurveDescription(CoordinateSystemType coordinateSystem)
        {
            this.coordinateSystem = coordinateSystem;
        }

        #region ICurveDescription implementation
#if !MICRO_FRAMEWORK
        IList<IAxisDescription> ICurveDescription.Axes
        {
            get
            {
                return ListCastAdapter<AxisDescription, IAxisDescription>.Build(Axes);
            }
        }
#endif

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public string CoordinateSystem
        {
            get
            {
                return (string)CoordinateSystemTypeConverter.Serialize(coordinateSystem);
            }
            set
            {
                coordinateSystem = CoordinateSystemTypeConverter.Deserialize(value);
            }
        }
#else
        [JsonProperty("coordinateSystem")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<CoordinateSystemType>))]
        [YAXSerializableField]
        [YAXSerializeAs("coordinateSystem")]
        [YAXCustomSerializer(typeof(EnumDescriptionSerializer<CoordinateSystemType>))]
        public CoordinateSystemType CoordinateSystem
        {
            get
            {
                return coordinateSystem;
            }
            set
            {
                coordinateSystem = value;
            }
        }
#endif
        #endregion

        /// <summary>
        /// Gets the base axes.
        /// </summary>
        /// <value>The base axes.</value>
        /// <seealso cref="IAxisDescription"/>
#if MICRO_FRAMEWORK
        protected abstract IList BaseAxes
#else
        protected abstract IList<ProbeNet.Messages.Base.AxisDescription> BaseAxes
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the axes.
        /// </summary>
        /// <value>The list of axis descriptions.</value>
        /// <seealso cref="IAxisDescription"></seealso>
#if MICRO_FRAMEWORK
        public IList Axes
#else
        [YAXDontSerialize]
        public IList<ProbeNet.Messages.Base.AxisDescription> Axes
#endif
        {
            get
            {
                return BaseAxes;
            }
        }
    }
}