/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if MICRO_FRAMEWORK
using System.Collections;
using ProbeNet.Enums.Converting;
#else
using System.Collections.Generic;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("measurementDescription")]
#endif
    public abstract class MeasurementDescription : MessageBody, IMeasurementDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.MeasurementDescription"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        protected MeasurementDescription(string id) :
            base(MessageType.MeasurementDescription)
        {
            Id = id;
        }

        /// <summary>
        /// Gets the caption.
        /// </summary>
        /// <value>The caption.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("caption")]
        [YAXSerializableField]
        [YAXSerializeAs("caption")]
        [YAXAttributeForClass]
#endif
        protected abstract string CaptionString
        {
            get;
        }

        /// <summary>
        /// Gets the short caption.
        /// </summary>
        /// <value>The short caption.</value>
#if !MICRO_FRAMEWORK
        [JsonProperty("shortCaption")]
        [YAXSerializableField]
        [YAXSerializeAs("shortCaption")]
        [YAXAttributeForClass]
#endif
        protected abstract string ShortCaptionString
        {
            get;
        }

        #region IMeasurementDescription implementation
        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("id")]
        [YAXSerializableField]
        [YAXSerializeAs("id")]
        [YAXAttributeForClass]
#endif
        public string Id
        {
            get;
            set;
        }

        string IMeasurementDescription.Caption
        {
            get
            {
                return CaptionString;
            }
        }

        string IMeasurementDescription.ShortCaption
        {
            get
            {
                return ShortCaptionString;
            }
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public IList TechnicalStandards
#else
        [JsonProperty("technicalStandards")]
        [YAXSerializableField]
        [YAXSerializeAs("technicalStandards")]
        [YAXCollection(YAXCollectionSerializationTypes.Recursive, EachElementName = "technicalStandard")]
        public IList<string> TechnicalStandards
#endif
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the base alignment description.
        /// </summary>
        /// <value>The base alignment description.</value>
        /// <seealso cref="IAlignmentDescription"/>
        protected abstract AlignmentDescription BaseAlignmentDescription
        {
            get;
        }

        /// <summary>
        /// Gets the alignment description.
        /// </summary>
        /// <value>The alignment description.</value>
        /// <seealso cref="IAlignmentDescription"/>
#if !MICRO_FRAMEWORK
        [YAXDontSerialize]
#endif
        public IAlignmentDescription AlignmentDescription
        {
            get
            {
                return BaseAlignmentDescription;
            }
        }

#if !MICRO_FRAMEWORK
        IList<IParameterDescription> IMeasurementDescription.ParameterDescriptions
        {
            get
            {
                return ListCastAdapter<ParameterDescription, IParameterDescription>.Build(ParameterDescriptions);
            }
        }
#endif

        /// <summary>
        /// Gets the base parameter descriptions.
        /// </summary>
        /// <value>The base parameter descriptions.</value>
        /// <seealso cref="IParameterDescription"/>
#if MICRO_FRAMEWORK
        protected abstract IList BaseParameterDescriptions
#else
        protected abstract IList<ParameterDescription> BaseParameterDescriptions
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the parameter descriptions.
        /// </summary>
        /// <value>The parameter descriptions.</value>
        /// <seealso cref="IParameterDescription"/>
#if MICRO_FRAMEWORK
        public IList ParameterDescriptions
#else
        [YAXDontSerialize]
        public IList<ParameterDescription> ParameterDescriptions
#endif
        {
            get
            {
                return BaseParameterDescriptions;
            }
        }

#if !MICRO_FRAMEWORK
        IList<IResultDescription> IMeasurementDescription.ResultDescriptions
        {
            get
            {
                return ListCastAdapter<ResultDescription, IResultDescription>.Build(ResultDescriptions);
            }
        }
#endif

        /// <summary>
        /// Gets the base result descriptions.
        /// </summary>
        /// <value>The base result descriptions.</value>
        /// <seealso cref="IResultDescription"/>
#if MICRO_FRAMEWORK
        protected abstract IList BaseResultDescriptions
#else
        protected abstract IList<ResultDescription> BaseResultDescriptions
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the result descriptions.
        /// </summary>
        /// <value>The result descriptions.</value>
        /// <seealso cref="IResultDescription"/>
#if MICRO_FRAMEWORK
        public IList ResultDescriptions
#else
        [YAXDontSerialize]
        public IList<ResultDescription> ResultDescriptions
#endif
        {
            get
            {
                return BaseResultDescriptions;
            }
        }

        /// <summary>
        /// Gets the base curve description.
        /// </summary>
        /// <value>The base curve description.</value>
        /// <seealso cref="ICurveDescription"/>
        protected abstract CurveDescription BaseCurveDescription
        {
            get;
        }

        /// <summary>
        /// Gets the curve description.
        /// </summary>
        /// <value>The curve description.</value>
        /// <seealso cref="ICurveDescription"/>
#if !MICRO_FRAMEWORK
        [YAXDontSerialize]
#endif
        public ICurveDescription CurveDescription
        {
            get
            {
                return BaseCurveDescription;
            }
        }

#if !MICRO_FRAMEWORK
        IList<IImageDescription> IMeasurementDescription.ImageDescriptions
        {
            get
            {
                return ListCastAdapter<ImageDescription, IImageDescription>.Build(ImageDescriptions);
            }
        }
#endif

        /// <summary>
        /// Gets the base image descriptions.
        /// </summary>
        /// <value>The base image descriptions.</value>
        /// <seealso cref="IImageDescription"/>
#if MICRO_FRAMEWORK
        protected abstract IList BaseImageDescriptions
#else
        protected abstract IList<ImageDescription> BaseImageDescriptions
#endif
        {
            get;
        }

        /// <summary>
        /// Gets the image descriptions.
        /// </summary>
        /// <value>The image descriptions.</value>
        /// <seealso cref="IImageDescription"/>
#if MICRO_FRAMEWORK
        public IList ImageDescriptions
#else
        [YAXDontSerialize]
        public IList<ImageDescription> ImageDescriptions
#endif
        {
            get
            {
                return BaseImageDescriptions;
            }
        }
        #endregion
    }
}
