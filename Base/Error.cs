/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;
using ProbeNet.Enums;
#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
#endif

namespace ProbeNet.Messages.Base
{
    /// <summary>
    /// The message body for an error message.
    /// </summary>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
#endif
    public abstract class Error : MessageBody, IError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Error"/> class.
        /// </summary>
        /// <param name="errorType">Error type.</param>
        protected Error(ErrorType errorType) :
            base(MessageType.Error)
        {
            ErrorType = errorType;
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        protected abstract string MessageString
        {
            get;
        }

        /// <inheritdoc/>
#if !MICRO_FRAMEWORK
        [JsonProperty("errorType")]
        [JsonConverter(typeof(JsonDescriptionEnumConverter<ErrorType>))]
#endif
        public ErrorType ErrorType
        {
            get;
            set;
        }

#if !MICRO_FRAMEWORK
        [JsonProperty("message")]
#endif
        string IError.Message
        {
            get
            {
                return MessageString;
            }
        }
    }
}