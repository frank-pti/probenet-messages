using ProbeNet.Enums;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using ProbeNet.Messages.Interface;
#if !MICRO_FRAMEWORK
using Frank.Helpers.Json;
using Newtonsoft.Json;
using YAXLib;
#endif

namespace ProbeNet.Messages.Base
{
    /// <inheritdoc/>
#if !MICRO_FRAMEWORK
    [JsonObject(MemberSerialization.OptIn)]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects, FieldsToSerialize = YAXSerializationFields.AttributedFieldsOnly)]
    [YAXSerializeAs("alignment")]
#endif
    public abstract class Alignment : IAlignment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Alignment"/> class.
        /// </summary>
        protected Alignment() :
#if MICRO_FRAMEWORK
            this(SampleSide.Top, 0, false, false)
#else
            this(null, null)
#endif
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Alignment"/> class.
        /// </summary>
        /// <param name="side">The side.</param>
        /// <param name="direction">The direction.</param>
#if MICRO_FRAMEWORK
        protected Alignment(SampleSide side, int direction) :
            this(side, direction, true, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Messages.Base.Alignment"/> class.
        /// </summary>
        /// <param name="side">The side.</param>
        /// <param name="direction">The direction.</param>
        /// <param name="sideValid">Whether the side value is valid</param>
        /// <param name="directionValid">Whether the direction value is valid</param>
        protected Alignment(SampleSide side, int direction, bool sideValid, bool directionValid)
#else
        protected Alignment(SampleSide? side, int? direction)
#endif
        {
            Direction = direction;
            Side = side;
#if MICRO_FRAMEWORK
            SideValid = sideValid;
            DirectionValid = directionValid;
#endif
        }

        #region IEquatable implementation
        /// <summary>
        /// Determines whether the specified <see cref="ProbeNet.Messages.Interface.IAlignment"/> is equal to the
        /// current <see cref="ProbeNet.Messages.Base.Alignment"/>.
        /// </summary>
        /// <param name="other">The <see cref="ProbeNet.Messages.Interface.IAlignment"/> to compare with the current <see cref="ProbeNet.Messages.Base.Alignment"/>.</param>
        /// <returns><c>true</c> if the specified <see cref="ProbeNet.Messages.Interface.IAlignment"/> is equal to the current
        /// <see cref="ProbeNet.Messages.Base.Alignment"/>; otherwise, <c>false</c>.</returns>
        public bool Equals(IAlignment other)
        {
#if MICRO_FRAMEWORK
            bool sideEquals = false;
            if (SideValid == other.SideValid) {
                if (SideValid) {
                    sideEquals = (Side == other.Side);
                } else {
                    sideEquals = true;
                }
            }
            bool directionEquals = false;
            if (DirectionValid == other.DirectionValid) {
                if (DirectionValid) {
                    directionEquals = (Direction == other.Direction);
                } else {
                    directionEquals = true;
                }
            }
            return sideEquals && directionEquals;
#else
            return Side == other.Side && Direction == other.Direction;
#endif
        }
        #endregion

        #region IAlignment implementation
        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public bool DirectionValid
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public int Direction
#else
        [JsonProperty("direction", NullValueHandling = NullValueHandling.Ignore)]
        [YAXSerializeAs("direction")]
        [YAXSerializableField]
        public int? Direction
#endif
        {
            get;
            set;
        }

        /// <inheritdoc/>
#if MICRO_FRAMEWORK
        public bool SideValid
        {
            get;
            set;
        }

        /// <inheritdoc/>
        public SampleSide Side
#else
        [JsonProperty("side", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(JsonDescriptionNullableEnumConverter<SampleSide>))]
        [YAXSerializeAs("side")]
        [YAXSerializableField]
        [YAXCustomSerializer(typeof(NullableEnumDescriptionSerializer<SampleSide>))]
        public SampleSide? Side
#endif
        {
            get;
            set;
        }
        #endregion
    }
}