#if !MICRO_FRAMEWORK
using Internationalization;
using ProbeNet.Enums;
using ProbeNet.Messages.Interface;
/*
 * Data types for the ProbeNet Protocol in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using TranslatableInterface = ProbeNet.Messages.Translatable.Interface;

namespace ProbeNet.Messages
{
    /// <summary>
    /// Helper class for parameters.
    /// </summary>
    public static class ParameterHelper
    {
        /// <summary>
        /// Extract parameters from measurement description.
        /// </summary>
        /// <returns>The parameters.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="description">Measurement description.</param>
        public static IDictionary<TranslationString, IParameterDescription> ExtractParameters(I18n i18n, IMeasurementDescription description)
        {
            Dictionary<TranslationString, IParameterDescription> result = new Dictionary<TranslationString, IParameterDescription>();
            if (description.ParameterDescriptions == null) {
                return result;
            }
            foreach (IParameterDescription parameterDescription in description.ParameterDescriptions) {
                TranslationString caption = null;
                TranslationString unit = null;
                if (parameterDescription is TranslatableInterface.ITranslatableValueDescription) {
                    TranslatableInterface.ITranslatableValueDescription translatableDescription =
                        parameterDescription as TranslatableInterface.ITranslatableValueDescription;
                    caption = translatableDescription.Caption;
                    unit = translatableDescription.Unit;
                } else {
                    caption = i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0}", parameterDescription.Caption);
                    unit = i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0}", parameterDescription.Unit);
                }

                if (String.IsNullOrWhiteSpace(unit.Tr)) {
                    result.Add(caption, parameterDescription);
                } else {
                    result.Add(i18n.TrObject(Constants.ProbeNetMessagesDomain, "{0} [{1}]", caption, unit), parameterDescription);
                }
            }
            return result;
        }

        /// <summary>
        /// Formats the parameter.
        /// </summary>
        /// <returns>The formatted parameter.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="rawValue">Raw value.</param>
        /// <param name="parameterDescription">Parameter description.</param>
        public static string FormatParameter(I18n i18n, object rawValue, IParameterDescription parameterDescription)
        {
            string v = String.Empty;
            switch (parameterDescription.Type) {
                case ParameterType.Boolean:
                    v = (bool)rawValue ?
                        i18n.Tr(Constants.ProbeNetMessagesDomain, "Yes") : i18n.Tr(Constants.ProbeNetMessagesDomain, "No");
                    break;
                case ParameterType.Enumeration:
                    v = LoadEnumValueCaption((string)rawValue, parameterDescription);
                    break;
                case ParameterType.Number:
                    string format = String.Format("F{0}", parameterDescription.DecimalPlaces);
                    if (rawValue != null) {
                        v = Convert.ToDouble(rawValue).ToString(format, i18n.NumberFormat);
                    } else {
                        v = String.Format(i18n.NumberFormat, "{0}", double.NaN);
                    }
                    break;
            }
            return v;
        }

        private static string LoadEnumValueCaption(string enumValue, IParameterDescription description)
        {
            foreach (IEnumerationValue e in description.EnumValues) {
                if (e.Id == enumValue) {
                    return e.Caption;
                }
            }
            return enumValue;
        }
    }
}
#endif
